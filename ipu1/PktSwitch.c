/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "ti/csl/cslr_device.h"

#include <icss_emacDrv.h>

#define BYTESWAP16(x)	((((x) >> 8) & 0xff) | (((x) & 0xff) << 8))
#define ETH_ALEN	6
#define ETH_VLAN	0x8100
#define ETH_TYPE_GOOSE	0x88B8
#define ETH_TYPE_PTP 	0x88F7
#define ETH_TYPE_SV	0x88BA

#define ACCEPT_APPID_NUM	4
#define DEST_MAC_NUM		4

char vlan;
extern uint32_t bGooseFilter;
extern char ptpMulDest1MacAddr[ETH_ALEN];
extern char ptpMulDest2MacAddr[ETH_ALEN];
extern char GooseDestMacAddr[DEST_MAC_NUM][ETH_ALEN];
extern uint16_t GooseAPPID[ACCEPT_APPID_NUM];

/**
* @internal
* @brief GOOSE filtering algorithm
*
* @param pIcssRxPkt: received packet data pointer
*
* @retval packet type 
*/

int32_t GooseProc(char *pIcssRxPkt)
{
	int32_t i, appId;
	int32_t bMac;

	if(vlan == 1)
		appId = *(uint16_t *)((char *)pIcssRxPkt + 2 * 
				ETH_ALEN + 2 + 4); 
	else 
		appId = *(uint16_t *)((char *)pIcssRxPkt + 2 * 
				ETH_ALEN + 2); 

	appId = BYTESWAP16(appId);

	if (bGooseFilter == 1) {
		bMac = 0;
		for ( i = 0; i < DEST_MAC_NUM; i++ ) {
			if (memcmp(pIcssRxPkt, GooseDestMacAddr[i], 
						ETH_ALEN) == 0) {
				bMac = 1;
				break;
			}
		}

		if (bMac == 1) {
			/* Drop a GOOSE packet not having APPID from the list of
			   acceptable APPIDs */
			for (i = 0; i < ACCEPT_APPID_NUM; i++) {
				if((appId == GooseAPPID[i]) && (appId < 0x3FFF))
					return PKT_PROC_GOOSE;
				else if (appId == GooseAPPID[i]) 
					return PKT_PROC_GOOSE;
			}
			return PKT_PROC_NONE;
		}
		else return PKT_PROC_MPU;
	}

	else
		if (appId < 0x3FFF)
			return PKT_PROC_GOOSE;
		else 
			return PKT_PROC_GOOSE;  

}

int32_t PtpProc(char *pIcssRxPkt)
{
	if((memcmp(pIcssRxPkt, ptpMulDest1MacAddr, ETH_ALEN) == 0) ||
		(memcmp(pIcssRxPkt, ptpMulDest2MacAddr, ETH_ALEN) == 0))
		return PKT_PROC_PTP;
	 else
		return PKT_PROC_NONE;

}

int32_t SvProc(char *pIcssRxPkt)
{
	//Log_print0(Diags_INFO, "Placeholder for SV packet filtering");
	return PKT_PROC_NONE;	
}

int32_t Filter2(char *pIcssRxPkt)
{
	//Log_print0(Diags_INFO, "Placeholder for additional filtering");
	return PKT_PROC_NONE;	
}

/**
* @internal
* @brief packet switch logic
*
* @param pIcssRxPkt: received packet data pointer
*
* @retval packet type 
*/
int32_t PruSwitch(char *pIcssRxPkt)
{
	uint16_t ethType;

	ethType = *(uint16_t *)(pIcssRxPkt  +  2 * ETH_ALEN);
	ethType = BYTESWAP16(ethType);

	if (ethType == ETH_VLAN) {
		vlan = 1;
		ethType = *(uint16_t *)((char *)pIcssRxPkt  +  2 * 
				ETH_ALEN + 2 + 2);
	}	
	else {
		vlan = 0;
		ethType = *(uint16_t *)((char *)pIcssRxPkt  +  2 * ETH_ALEN);
	}
	ethType = BYTESWAP16(ethType);

	switch (ethType) {
		case ETH_TYPE_GOOSE: 
			return GooseProc(pIcssRxPkt);
		case ETH_TYPE_PTP:
			return PtpProc(pIcssRxPkt);
		case ETH_TYPE_SV:
			return SvProc(pIcssRxPkt);
		default: 
			Filter2(pIcssRxPkt);
			return PKT_PROC_MPU;
	}
}

