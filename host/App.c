/*
 * Copyright (c) 2013-2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== App.c ========
 *
 */

/* host header files */
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>
#include <ti/cmem.h>

/* local header files */
#include "../shared/AppCommon.h"
#include "App.h"

#include <time.h>

/* module structure */
typedef struct {
	MessageQ_Handle         hostQue;    // created locally
	MessageQ_QueueId        slaveQue;   // opened remotely
	UInt16                  heapId;     // MessageQ heapId
	UInt32                  msgSize;
} App_Module;

/* private data */
static App_Module Module;


typedef struct  bufmgrDesc_s {
	UInt32 physAddr;            /* physical address */
	char *userAddr;             /* Host user space Virtual address */
	UInt32 length;              /* Length of host buffer */

} bufmgrDesc_t;


#define MAX_TEST_MSG_NUM  256

#define TEST_PORT 0
#define MAX_MII_PORTS_NUM 3
#define MAX_PACKET_FRAME_SIZE 1536 

//#define GOOSE_FILTER_DISABLE
#define GOOSE_PARA_LEN 1024

/* testing packets from host if needed */
#define TEST_PKT_SIZE       64
static const char test_pkt[2][TEST_PKT_SIZE] = {
	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac */
		0x01, 0x55, 0x55, 0x55, 0x55, 0x55,
		0x08, 0x06, 0x00, 0x01,
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},
	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac */
		0x01, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
		0x08, 0x06, 0x00, 0x01,
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	}
};

CMEM_AllocParams  alloc_params;
bufmgrDesc_t  cmem_buf_desc;

bufmgrDesc_t  RxPacketBuf[MAX_MII_PORTS_NUM][2];
bufmgrDesc_t  TxPacketBuf[MAX_MII_PORTS_NUM][2];
bufmgrDesc_t  GoosePara;

char *pGoosePara;

/*
 *  ======== parse /etc/goose if available for filtering parameters ========
 */
Void Read_Goose_para()
{
	FILE *fp;
	unsigned int gooseFlag;
	unsigned char destMacAddr[4][6];
	unsigned short appId[4];
	int i;


	fp = fopen("/etc/goose", "r");
	if(fp == NULL) {
		printf("/etc/goose doesn't exsit," 
				"use default goose parameters\n");
		return;
	}

	fscanf(fp, "%x", &gooseFlag);

	pGoosePara = GoosePara.userAddr;
	memcpy(pGoosePara, &gooseFlag, 4);

	for (i=0; i<4; i++)
		fscanf(fp, "%x %x %x %x %x %x", 
				(unsigned int *)&destMacAddr[i][0],
				(unsigned int *)&destMacAddr[i][1],
				(unsigned int *)&destMacAddr[i][2],
				(unsigned int *)&destMacAddr[i][3],
				(unsigned int *)&destMacAddr[i][4],
				(unsigned int *)&destMacAddr[i][5]);
	memcpy(pGoosePara + 4, &destMacAddr[0][0], 24);

	fscanf(fp, "%x %x %x %x", (unsigned int *)&appId[0],
			(unsigned int *)&appId[1],
			(unsigned int *)&appId[2],
			(unsigned int *)&appId[3]);

	memcpy(pGoosePara + 28, &appId[0], 16);

	printf("GooseFlag = 0x%x\n", *(unsigned int *)pGoosePara);

	for(i=0; i<4; i++)
		printf("Goose destMacAddr[%d] =" 
				"0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n", i, 
				*(pGoosePara + i*6 + 4),
				*(pGoosePara + i*6 + 5),
				*(pGoosePara + i*6 + 6),
				*(pGoosePara + i*6 + 7),
				*(pGoosePara + i*6 + 8),
				*(pGoosePara + i*6 + 9));

	printf("Goose AppID = 0x%x 0x%x 0x%x 0x%x\n", 
			*(unsigned short *)(pGoosePara + 28),
			*(unsigned short *)(pGoosePara + 30),
			*(unsigned short *)(pGoosePara + 32),
			*(unsigned short *)(pGoosePara + 34));
	fclose(fp);						
}

/*
 *  ======== initialize CMEM buffers ========
 */
Void initCmemBufs()
{
	CMEM_AllocParams  alloc_params;
	Int i;

	printf("--->App_Create: CMEM_allocPhys and map\n");
	alloc_params.flags = CMEM_NONCACHED;
	alloc_params.type = CMEM_POOL;
	alloc_params.alignment = 0;
	if(CMEM_init() != 0)
		printf("--->App_Create: ERROR: CMEM_init()\n");

	cmem_buf_desc.physAddr = CMEM_allocPhys(4 * MAX_MII_PORTS_NUM * 
			MAX_PACKET_FRAME_SIZE + GOOSE_PARA_LEN, &alloc_params);

	if(cmem_buf_desc.physAddr == 0 )
		printf("--->App_Create: ERROR: CMEM_allocPhys()\n");
	else
		printf("--->App_Create: cmem_buf_desc.physAddr = 0x%x\n", 
				cmem_buf_desc.physAddr);

	cmem_buf_desc.length = 4 * MAX_MII_PORTS_NUM * MAX_PACKET_FRAME_SIZE + 
		GOOSE_PARA_LEN;

	cmem_buf_desc.userAddr = CMEM_map((UInt32)cmem_buf_desc.physAddr, 
			cmem_buf_desc.length);
	if(cmem_buf_desc.userAddr == NULL)
		printf("--->App_Create: ERROR: CMEM_map()\n");

	for (i = 0; i < MAX_MII_PORTS_NUM; i++)
	{ 
		RxPacketBuf[i][0].length = MAX_PACKET_FRAME_SIZE;
		RxPacketBuf[i][0].userAddr = 
			(char *)((UInt32)(cmem_buf_desc.userAddr + 
						(i*4+0)*MAX_PACKET_FRAME_SIZE));

		RxPacketBuf[i][0].physAddr = (UInt32)((cmem_buf_desc.physAddr +
					(i*4+0)*MAX_PACKET_FRAME_SIZE));

		RxPacketBuf[i][1].length = MAX_PACKET_FRAME_SIZE;
		RxPacketBuf[i][1].userAddr = 
			(char *)((UInt32)(cmem_buf_desc.userAddr + 
						(i*4+1)*MAX_PACKET_FRAME_SIZE));

		RxPacketBuf[i][1].physAddr = (UInt32)((cmem_buf_desc.physAddr +
					(i*4+1)*MAX_PACKET_FRAME_SIZE));

		TxPacketBuf[i][0].length = MAX_PACKET_FRAME_SIZE;
		TxPacketBuf[i][0].userAddr = 
			(char *)((UInt32)cmem_buf_desc.userAddr + 
					(i*4+2)*MAX_PACKET_FRAME_SIZE);

		TxPacketBuf[i][0].physAddr = (UInt32)((cmem_buf_desc.physAddr + 
					(i*4+2)*MAX_PACKET_FRAME_SIZE)); 


		TxPacketBuf[i][1].length = MAX_PACKET_FRAME_SIZE;
		TxPacketBuf[i][1].userAddr = 
			(char *)((UInt32)cmem_buf_desc.userAddr + 
					(i*4+3)*MAX_PACKET_FRAME_SIZE);

		TxPacketBuf[i][1].physAddr = (UInt32)((cmem_buf_desc.physAddr + 
					(i*4+3)*MAX_PACKET_FRAME_SIZE)); 

	}

	GoosePara.length = GOOSE_PARA_LEN;
	GoosePara.userAddr = (char *)((UInt32)cmem_buf_desc.userAddr + 
			MAX_MII_PORTS_NUM*4*MAX_PACKET_FRAME_SIZE);
	GoosePara.physAddr = (UInt32)(cmem_buf_desc.physAddr + 
			MAX_MII_PORTS_NUM*4*MAX_PACKET_FRAME_SIZE); 

	Read_Goose_para();
}

/*
 *  ======== App_create ========
 */

Int App_create(UInt16 remoteProcId)
{
	Int                 status = 0;
	MessageQ_Params     msgqParams;
	char                msgqName[32];

	printf("--> App_create:\n");

	/* setting default values */
	Module.hostQue = NULL;
	Module.slaveQue = MessageQ_INVALIDMESSAGEQ;
	Module.heapId = App_MsgHeapId;
	Module.msgSize = sizeof(App_Msg);

	initCmemBufs();
	/* create local message queue (inbound messages) */
	MessageQ_Params_init(&msgqParams);

	Module.hostQue = MessageQ_create(App_HostMsgQueName, &msgqParams);

	if (Module.hostQue == NULL) {
		printf("App_create: Failed creating MessageQ\n");
		status = -1;
		goto leave;
	}

	/* open the remote message queue */
	sprintf(msgqName, App_SlaveMsgQueName, MultiProc_getName(remoteProcId));

	do {
		status = MessageQ_open(msgqName, &Module.slaveQue);
		sleep(1);
	} while (status == MessageQ_E_NOTFOUND);

	if (status < 0) {
		printf("App_create: Failed opening MessageQ\n");
		goto leave;
	}

	printf("App_create: Host is ready\n");

leave:
	printf("<-- App_create:\n");
	return(status);
}


/*
 *  ======== App_delete ========
 */
Int App_delete(Void)
{
	Int         status;

	printf("--> App_delete:\n");

	/* close remote resources */
	status = MessageQ_close(&Module.slaveQue);

	if (status < 0) {
		goto leave;
	}

	/* delete the host message queue */
	status = MessageQ_delete(&Module.hostQue);

	if (status < 0) {
		goto leave;
	}

leave:
	printf("<-- App_delete:\n");
	return(status);
}


/*
 *  ======== App_exec ========
 */
Int App_exec(Void)
{
	Int         status;
	Int 	ethPort, nPktBuf, pktLen;
	char 	*pRxPkt;
	char 	*pTxPkt, *pTxPkt0, *pTxPkt1;
	Int         i,j;
	App_Msg *   msg;

	printf("--> App_exec:\n");

	/* fill process pipeline */
	for (i = 1; i <= 4; i++) {
		printf("App_exec: sending message %d\n", i);

		/* allocate message */
		msg = (App_Msg *)MessageQ_alloc(Module.heapId, Module.msgSize);

		if (msg == NULL) {
			status = -1;
			goto leave;
		}

		/* set the return address in the message header */
		MessageQ_setReplyQueue(Module.hostQue, (MessageQ_Msg)msg);

		/* fill in message payload */
		msg->cmd = App_CMD_NOP;

#ifdef GOOSE_FILTER_DISABLE	
		/* disable Goose filtering */
		if (i == 3) 
			msg->cmd = App_CMD_GOOSE_DISABLE;
#endif

		if (i == 4) {
			msg->cmd = App_CMD_CMEMBUF;
			msg->port = 0;
			msg->nRxBuf = 0;
			for (j = 0; j < MAX_MII_PORTS_NUM; j++) {
				msg->RxPhysAddr[j][0] = 
					RxPacketBuf[j][0].physAddr;
				msg->RxPktLen[j][0] = 0;

				msg->RxPhysAddr[j][1] = 
					RxPacketBuf[j][1].physAddr;
				msg->RxPktLen[j][1] = 0;

				msg->TxPhysAddr[j][0] = 
					TxPacketBuf[j][0].physAddr;
				msg->TxPktLen[j][0] = 0;

				msg->TxPhysAddr[j][1] = 
					TxPacketBuf[j][1].physAddr;
				msg->TxPktLen[j][1] = 0;
			}
			msg->GooseParaPhysAddr = GoosePara.physAddr;

			pTxPkt0 = TxPacketBuf[TEST_PORT][0].userAddr;
			pTxPkt1 = TxPacketBuf[TEST_PORT][1].userAddr;
			pTxPkt = pTxPkt0;
		}

		/* send message */
		MessageQ_put(Module.slaveQue, (MessageQ_Msg)msg);
	}

	/* process steady state (keep pipeline full) */
	for (i = 5; i <= MAX_TEST_MSG_NUM; i++) {

		/* wait for return message */
		status = MessageQ_get(Module.hostQue, (MessageQ_Msg *)&msg,
				MessageQ_FOREVER);

		if (status < 0) {
			goto leave;
		}

		/* extract message payload */
		if(msg->cmd == App_CMD_PACKET)
		{
			printf("App_exec: packet received: \n");
			ethPort = msg->port;
			nPktBuf = msg->nRxBuf;
			pktLen = msg->RxPktLen[ethPort][nPktBuf];

			pRxPkt = (char *)RxPacketBuf[ethPort][nPktBuf].userAddr;

			for(j=0; j<msg->RxPktLen[ethPort][nPktBuf]; j++)
				printf("%02x", *(pRxPkt+j));

			printf("\nreceived packet frame size: %d\n", pktLen);
		}

		/* free the message */
		MessageQ_free((MessageQ_Msg)msg);

		printf("\nApp_exec: message received, sending message %d\n", i);

		/* allocate message */
		msg = (App_Msg *)MessageQ_alloc(Module.heapId, Module.msgSize);

		if (msg == NULL) {
			status = -1;
			goto leave;
		}

		/* set the return address in the message header */
		MessageQ_setReplyQueue(Module.hostQue, (MessageQ_Msg)msg);

		/* fill in message payload */
		if (i == MAX_TEST_MSG_NUM) {
			/* Last message will tell the slave to shutdown */
			msg->cmd = App_CMD_SHUTDOWN;
		}
		else {
			msg->cmd = App_CMD_PACKET;
		}

		/* send message */
		if(pTxPkt == pTxPkt0) {
			msg->TxPktLen[TEST_PORT][0] = TEST_PKT_SIZE;
			memcpy(pTxPkt, test_pkt[0], TEST_PKT_SIZE);
			msg->nTxBuf = 0;
			pTxPkt = pTxPkt1;
		}
		else {
			msg->TxPktLen[TEST_PORT][1] = TEST_PKT_SIZE;
			memcpy(pTxPkt, test_pkt[1], TEST_PKT_SIZE);
			msg->nTxBuf = 1;
			pTxPkt = pTxPkt0;
		}
		MessageQ_put(Module.slaveQue, (MessageQ_Msg)msg);
	}

	/* drain process pipeline */
	for (i = 1; i <= 4; i++) {
		printf("App_exec: message received\n");

		/* wait for return message */
		status = MessageQ_get(Module.hostQue, (MessageQ_Msg *)&msg,
				MessageQ_FOREVER);

		if (status < 0) {
			goto leave;
		}

		/* extract message payload */
		if(msg->cmd == App_CMD_PACKET)
		{
			printf("App_exec: packet received: \n");
			ethPort = msg->port;
			nPktBuf = msg->nRxBuf;
			pktLen = msg->RxPktLen[ethPort][nPktBuf];

			pRxPkt = (char *)RxPacketBuf[ethPort][nPktBuf].userAddr;

			for(j=0; j<msg->RxPktLen[ethPort][nPktBuf]; j++)
				printf("%02x", *(pRxPkt+j));

			printf("\nreceived packet frame size: %d\n", pktLen);

		}

		/* free the message */
		MessageQ_free((MessageQ_Msg)msg);
	}

leave:
	CMEM_unmap(cmem_buf_desc.userAddr, cmem_buf_desc.length);
	CMEM_freePhys(cmem_buf_desc.physAddr, &alloc_params);

	printf("<-- App_exec: %d\n", status);
	return(status);
}
