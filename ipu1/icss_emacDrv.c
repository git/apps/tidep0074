/**
 * @file icss_emacDrv.c
 * @brief Contains the Rx and Tx functions for packet processing on ARM including the ISR for two PRU
 *
 */

/* Copyright (C) 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*
*   Redistribution and use in source and binary forms, with or without 
*   modification, are permitted provided that the following conditions 
*   are met:
*
*     Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer.
*
*     Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the   
*     distribution.
*
*     Neither the name of Texas Instruments Incorporated nor the names of
*     its contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/



/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <stdint.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/knl/Cache.h>

#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>

#include <ti/csl/src/ip/icss/V0/cslr_icss_intc.h>
#include <ti/csl/src/ip/mdio/V2/cslr_mdio.h>
#include <ti/csl/src/ip/mdio/V2/csl_mdio.h>
#include <ti/csl/src/ip/mdio/V2/csl_mdioAux.h>

#include <ti/drv/pruss/pruicss.h>

//#include <ti/drv/icss_emac/icss_emacDrv.h>
#include <icss_emacDrv.h>
#include <ti/drv/icss_emac/icss_emacCommon.h>
#include <ti/drv/icss_emac/icss_emacStatistics.h>
#include <ti/drv/icss_emac/icss_emacStormControl.h>
#include <ti/drv/icss_emac/icss_emacLearning.h>
#include <ti/drv/icss_emac/icss_emacFwInit.h>
#include <ti/drv/icss_emac/icss_emac_osal.h>

extern int32_t PruSwitch(char *pIcssRxPkt);
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/**This value in the MDIO Reg means 10 mbps mode is enabled*/
#define Ten_Mbps  0xa
/**This value in the MDIO Reg means 100 mbps mode is enabled*/
#define Hundread_Mbps 0x64
/**Minimum supported size of Ethernet frame*/
#define ETHERNET_FRAME_SIZE_60 60

#define LINK0_PRU_EVT_MASK  (0x200U)
#define LINK1_PRU_EVT_MASK  (0x200000U)
/**Pacing timer Value*/
#define DEFAULT_PACING_TIMER_VAL 100

#define ENABLE_TIMER_SUPPORT

/**Timer Handle for pacing*/
#ifdef ENABLE_TIMER_SUPPORT
void* pacingTimerHandle;
#endif

#define PHY_LINK_STATUS                   (0x0004u)
#define PHY_BSR                           (1u)
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */


/** Variable containing list of implemented protocols*/
uint16_t numImplementedProtocols = NUM_PROTOCOLS_IMPLEMENTED;
/**list of identified protocol types, rest initialized to zero*/
uint16_t protocol_impl[MAX_NUM_PROTOCOL_IMPLEMENTED] = {IP4_PROT_TYPE, ARP_PROT_TYPE,0,0,0,0, 0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
                                                        0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0};
/** port params for three ports, two physical and one for host*/
//ICSS_EmacPortParams switchPort[3];
/**Number of collisions occured*/
int32_t num_of_collision_occured = 0;
/**Number of packets dropped as a result of collision not resolved*/
int32_t collision_pkt_dropped =0;
/** @brief User specific data HW Port 0 Link ISR*/


/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
/**
 *  \name ICSS_EmacClearRxIrq
 *  @brief Clears Rx interrupt
 *
 *  @param none
 *
 *  @retval none
 *
 */
inline void ICSS_EmacClearRxIrq(ICSS_EmacHandle icssemacHandle);   /* for misra warning*/
inline void ICSS_EmacClearRxIrq(ICSS_EmacHandle icssemacHandle)
{
    if((((ICSS_EmacObject*)icssemacHandle->object)->emacInitcfg)->portMask == ICSS_EMAC_MODE_MAC2) {
        HW_WR_FIELD32(((((ICSS_EmacHwAttrs*)icssemacHandle->hwAttrs)->emacBaseAddrCfg)->prussIntcRegs + CSL_ICSSINTC_SECR0),
                CSL_ICSSINTC_SECR0_ENA_STATUS_31_0, (1U) << 21);
    } else {
        HW_WR_FIELD32(((((ICSS_EmacHwAttrs*)icssemacHandle->hwAttrs)->emacBaseAddrCfg)->prussIntcRegs + CSL_ICSSINTC_SECR0),
                CSL_ICSSINTC_SECR0_ENA_STATUS_31_0, (1U) << 20);
    }
}

void ICSS_EmacEnableRxInterrupt(ICSS_EmacHandle icssemacHandle);   /* for misra warning*/
void ICSS_EmacEnableRxInterrupt(ICSS_EmacHandle icssemacHandle)
{
    uint32_t intNum = ((ICSS_EmacObject*)icssemacHandle->object)->emacInitcfg->rxIntNum;
    ICSS_EMAC_osalHardwareInterruptEnable((int32_t)intNum);
}

void ICSS_EmacDisableRxInterrupt(ICSS_EmacHandle icssemacHandle);  /* for misra warning*/
void ICSS_EmacDisableRxInterrupt(ICSS_EmacHandle icssemacHandle)
{
    uint32_t intNum = ((ICSS_EmacObject*)icssemacHandle->object)->emacInitcfg->rxIntNum;
    ICSS_EMAC_osalHardwareInterruptDisable((int32_t)intNum);

}


/**
 *  \name ICSS_EmacClearTxIrq
 *  @brief Clears Tx Packet Completion interrupt
 *
 *  @param none
 *
 *  @retval none
 *
 */
inline void ICSS_EmacClearTxIrq(ICSS_EmacHandle icssemacHandle); /* for misra warnings*/
inline void ICSS_EmacClearTxIrq(ICSS_EmacHandle icssemacHandle)
{
    if((((ICSS_EmacObject*)icssemacHandle->object)->emacInitcfg)->portMask == ICSS_EMAC_MODE_MAC2) {
        HW_WR_FIELD32(((((ICSS_EmacHwAttrs*)icssemacHandle->hwAttrs)->emacBaseAddrCfg)->prussIntcRegs + CSL_ICSSINTC_SECR0),
                CSL_ICSSINTC_SECR0_ENA_STATUS_31_0, (1U) << 23);
    } else {
        HW_WR_FIELD32(((((ICSS_EmacHwAttrs*)icssemacHandle->hwAttrs)->emacBaseAddrCfg)->prussIntcRegs + CSL_ICSSINTC_SECR0),
                CSL_ICSSINTC_SECR0_ENA_STATUS_31_0, (1U) << 22);
    }
}

void ICSS_EmacEnableTxInterrupt(ICSS_EmacHandle icssemacHandle); /* for misra warnings*/
void ICSS_EmacEnableTxInterrupt(ICSS_EmacHandle icssemacHandle) {
    uint32_t intNum = ((ICSS_EmacObject*)icssemacHandle->object)->emacInitcfg->txIntNum;
    ICSS_EMAC_osalHardwareInterruptEnable((int32_t)intNum);
}

void ICSS_EmacDisableTxInterrupt(ICSS_EmacHandle icssemacHandle); /* for misra warnings*/
void ICSS_EmacDisableTxInterrupt(ICSS_EmacHandle icssemacHandle) {
    uint32_t intNum = ((ICSS_EmacObject*)icssemacHandle->object)->emacInitcfg->txIntNum;
    ICSS_EMAC_osalHardwareInterruptDisable((int32_t)intNum);

}
/**
 *  \name ICSS_EmacTxInterruptHandler
 *  @brief Main Tx completion interrupt service routine
 *
 *  @param args arguments if any
 *
 *  @retval
 *       void
 *
 */
void ICSS_EmacTxInterruptHandler(void *args)
{
    ICSS_EmacHandle handle = (ICSS_EmacHandle)args;
    ICSS_EMAC_osalPostLock(((ICSS_EmacObject*)handle->object)->txSemaphoreHandle);
    ICSS_EmacClearTxIrq((ICSS_EmacHandle)args);
}
/**
 *  \name ICSS_EmacRxInterruptHandler
 *  @brief Main Rx interrupt service routine
 *
 *  @param args arguments if any
 *
 *  @retval
 *       void
 *
 */
void ICSS_EmacRxInterruptHandler(void *args)
{
    uint8_t pacingEnabled;
#ifdef ENABLE_TIMER_SUPPORT
    uint8_t pacingMode;
#endif
    ICSS_EmacHandle handle = (ICSS_EmacHandle)args;
    ICSS_EmacClearRxIrq((ICSS_EmacHandle)args);

    pacingEnabled = (((ICSS_EmacObject*)handle->object)->emacInitcfg)->enableIntrPacing;

#ifdef ENABLE_TIMER_SUPPORT
    pacingMode = (((ICSS_EmacObject*)handle->object)->emacInitcfg)->ICSS_EmacIntrPacingMode;
#endif
    /*disable Rx interrupt on ARM, PRU line stays high*/
    if(pacingEnabled == ICSS_EMAC_ENABLE_PACING) {
        ICSS_EmacDisableRxInterrupt(handle);

#ifdef ENABLE_TIMER_SUPPORT
        if(pacingMode == ICSS_EMAC_INTR_PACING_MODE2) {
            ICSS_EMAC_osalTimerStart(pacingTimerHandle);
        }
#endif
     }

    ICSS_EMAC_osalPostLock(((ICSS_EmacObject*)handle->object)->rxSemaphoreHandle);
}

/**
 *  \name ICSS_EMacOsRxTaskFnc
 *  @brief
 *      Function which pends on Rx semaphore.Gets the Rx packet info for processing
 *
 *  @param a0 arg 1
 *  @param a1 arg 2
 *
 *  @retval none
 *
 */
void ICSS_EMacOsRxTaskFnc(uint32_t a0, uint32_t a1)
{
    int32_t port_number;
    int32_t queue_number;
    int32_t pkt_proc;
    int16_t pLength;
    int32_t allQueuesEempty = 0;
    int8_t  dest_address[ICSS_EMAC_MAXMTU];
    uint16_t numPacketsInLoop = 0;
    uint8_t pacingEnabled;
    uint8_t pacingMode;
    ICSS_EmacHandle icssEmacHandle;
    icssEmacHandle = (ICSS_EmacHandle)a0;
    ICSS_EmacRxArgument rxArg;

    pacingEnabled = (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->enableIntrPacing;
    pacingMode    = (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->ICSS_EmacIntrPacingMode;

    while(1)
    {
        ICSS_EMAC_osalPendLock(((ICSS_EmacObject*)icssEmacHandle->object)->rxSemaphoreHandle, SemaphoreP_WAIT_FOREVER);

        while((allQueuesEempty != 1) && (numPacketsInLoop <= ((((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->pacingThreshold)))
        {
            pLength = ((int16_t)(ICSS_EmacRxPktInfo(icssEmacHandle, &port_number, &queue_number, &pkt_proc)));
            if(pLength > 0)
            {
                /*Ethernet packet will be put into ICSS_EMAC_QUEUEPRIO4 only
                 * Give the packet to ndk stack*/
                if(queue_number >= ((ICSS_EmacObject*)(icssEmacHandle->object))->emacInitcfg->ethPrioQueue)
                {
                      ((ICSS_EmacObject*)icssEmacHandle->object)->icssEmacHwIntRx(&queue_number,icssEmacHandle);
                } 
                else 
                {
                    if(((((ICSS_EmacObject*)icssEmacHandle->object)->callBackHandle)->rxRTCallBack)->callBack != NULL) {
                        ((((ICSS_EmacObject*)icssEmacHandle->object)->callBackHandle)->rxRTCallBack)->callBack(&queue_number,
                                ((((ICSS_EmacObject*)icssEmacHandle->object)->callBackHandle)->rxRTCallBack)->userArg);
                    } else {
                            rxArg.icssEmacHandle = icssEmacHandle;
                            rxArg.destAddress = (uint32_t)dest_address;
                            rxArg.more = 0;
                            rxArg.queueNumber = queue_number;
                            rxArg.port=port_number;

                            ((((ICSS_EmacObject *)icssEmacHandle->object)->callBackHandle)->rxCallBack)->callBack(&rxArg,NULL);   /* just dump the packet here so we do no stall the queues */
                    }

                }
                if(pacingEnabled == ICSS_EMAC_ENABLE_PACING) {
                    numPacketsInLoop++;
                }
            } 
            else 
            {
                allQueuesEempty =1;
            }
        }
        allQueuesEempty =0;

        if(pacingEnabled == ICSS_EMAC_ENABLE_PACING) {
            numPacketsInLoop = 0;
            /*Enable interrupts*/
            if(pacingMode == ICSS_EMAC_INTR_PACING_MODE1) {
                ICSS_EmacEnableRxInterrupt(icssEmacHandle);
            }
        }

    }
}
/**
 *  \name ICSS_EmacInterruptPacingISR
 *  @brief ISR for Interrupt Pacing DM Timer
 *
 *  @param args arguments if any
 *
 *  @retval
 *       void
 *
 */
void ICSS_EmacInterruptPacingISR(void *args); /* for misra warning*/
void ICSS_EmacInterruptPacingISR(void *args)
{
    ICSS_EmacHandle icssEmacHandle;
    icssEmacHandle = (ICSS_EmacHandle)args;
    ICSS_EmacEnableRxInterrupt(icssEmacHandle);
}

/**
 *  @b Description
 *  @n
 *      API to queue a frame which has to be transmitted on the
 *      specified port queue
 *
 *  @param[in]  txArg defined at @ref ICSS_EmacTxArgument
 *  @param[in]  userArg custom Tx packet callback packet options only required for custom TxPacket implementations,
                default to NULL when calling ICSS_EmacTxPacket which is default Tx Packet API
 *  @retval     0 on scuess,  <0 on failure
 */
int32_t ICSS_EmacTxPacket(ICSS_EmacTxArgument *txArg, void* userArg)

{
    int32_t ret=-1;
    const uint8_t*        macId;
    uint8_t txPort, trigTx1, trigTx2;

    ICSS_EmacHandle icssEmacHandle = txArg->icssEmacHandle;
    uint8_t portNumber= txArg->portNumber;
    uint8_t queuePriority = txArg->queuePriority;
    uint16_t lengthOfPacket = txArg->lengthOfPacket;

    
    int32_t ret_val = 0;

    macId = txArg->srcAddress;

    if (portNumber == ICSS_EMAC_PORT_0)    /* special case to use MAC learning */
    {
        /* get the Port number from MAC tables.. */
        txPort = findMAC(macId, ((ICSS_EmacObject*)(icssEmacHandle)->object)->macTablePtr);

        switch (txPort)
        {
            case 0U: /* need to send on both ports */
                trigTx1 = 1U;
                trigTx2 = 1U;
                break;
            case 1U: /* ICSS_EMAC_PORT_1 */
                trigTx1 = 1U;
                trigTx2 = 0U;
                break;
            case 2U: /* ICSS_EMAC_PORT_2 */
                trigTx1 = 0U;
                trigTx2 = 1U;
                break;
            default:    /* ICSS_EMAC_PORT_0 */
                trigTx1 = 1U;
                trigTx2 = 0U;
                break;
        }
        if(trigTx1 == 1U)
        {
            ret = ICSS_EmacTxPacketEnqueue(icssEmacHandle,
                                           txArg->srcAddress,
                                           ICSS_EMAC_PORT_1, 
                                           queuePriority,
                                           lengthOfPacket);
        }
        if(trigTx2 == 1U)
        {
            if (ret == 0) {
                ICSS_EmacTxPacketEnqueue( icssEmacHandle,
                                          txArg->srcAddress,
                                          ICSS_EMAC_PORT_2,
                                          queuePriority,
                                          lengthOfPacket);
            } else {
                ret = ICSS_EmacTxPacketEnqueue( icssEmacHandle,
                                                txArg->srcAddress,
                                                ICSS_EMAC_PORT_2,
                                                queuePriority,
                                                lengthOfPacket);
            }
        }

    }
    else
    {
        ret = ICSS_EmacTxPacketEnqueue(icssEmacHandle,
                                       txArg->srcAddress,
                                       portNumber,
                                       queuePriority,
                                       lengthOfPacket);
    }

    if(ret == 0)
    {
        ret_val = 0;
    }
    else
    {
        ret_val = -1;
    }

    return(ret_val);
}

/* Local Functions */
/**
 *  @brief  API to copy a packet from DDR to Tx Queue memory on L3 and synchronize with
 *  firmware
 *
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance.
 *  @param[in]  srcAddress    Base address of the buffer where the frame to be transmitted resides
 *  @param[in]  portNumber   Port on which frame has to be transmitted.
 *                            Valid values are:
                              1 == PORT1, 2 == PORT2
 *  @param[in]  queuePriority    Queue number in which frame will be
 *                               queued for transmission
 *  @param[in] lengthOfPacket   length of the frame in bytes
 *  @retval     0 on scuess,  <0 on failure
 */
int32_t ICSS_EmacTxPacketEnqueue (ICSS_EmacHandle icssEmacHandle,
                                  const uint8_t* srcAddress,
                                  uint8_t portNumber,
                                  uint8_t queuePriority,
                                  uint16_t lengthOfPacket)
{
    uint16_t buffer_offset_computed =0;
    uint32_t buffer_des = 0;
    uint16_t queue_wr_ptr =0;
    uint16_t wrk_queue_wr_ptr =0;
    uint16_t size = 0;
    uint16_t queue_rd_ptr =0;
    uint16_t num_of_bytes =0;
    uint16_t new_packet_length =0;
    uint32_t temp =0;
    uint16_t i =0;
    uint32_t collision_queue_selected =0;
    uint16_t collision_status =0;
    uint16_t col_queue_already_occupied =0;
    uint16_t original_length_of_packet =0;
    uint16_t packet_min_size_padding = 0;
    uint16_t remaining_valid_frame_data_length =0;
    uint8_t *macAddr;
    uint32_t pruSharedMem = 0;
    ICSS_EmacQueueParams *txQueue;
    ICSS_EmacHostStatistics_t* hostStatPtr;
    uint8_t linkStatus=0;

    uint8_t emacMode=0;

    uint32_t temp_addr = 0U;
    if((((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->portMask == ICSS_EMAC_MODE_SWITCH)
    {
        emacMode =0;
    }
    else
    {
        emacMode =1U;
    }

    if(emacMode) { /*MAC Mode*/

        if(ICSS_EMAC_PORT_1 == portNumber) {
            pruSharedMem = (((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam0BaseAddr;
        }
        if(ICSS_EMAC_PORT_2 == portNumber) {
            pruSharedMem = (((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr;
        }
        linkStatus = ((ICSS_EmacObject*)icssEmacHandle->object)->linkStatus[0];
        hostStatPtr = (ICSS_EmacHostStatistics_t*)((((ICSS_EmacObject*)icssEmacHandle->object)->hostStat));
    }
    else
    {
        linkStatus = ((ICSS_EmacObject*)icssEmacHandle->object)->linkStatus[portNumber-1];
        hostStatPtr = (ICSS_EmacHostStatistics_t*)((((ICSS_EmacObject*)icssEmacHandle->object)->hostStat));
        hostStatPtr += (portNumber - 1);/*Switch mode. Points to correct structure depending on port*/
    }

    if((portNumber != ICSS_EMAC_PORT_1) && (portNumber != ICSS_EMAC_PORT_2))
    {
        hostStatPtr->txDroppedPackets++;
        return ((int32_t)ICSS_EMAC_SWITCH_INVALID_PORT);
    }
    if((queuePriority < ICSS_EMAC_QUEUE1) || (queuePriority > ICSS_EMAC_QUEUE4))
    {
        hostStatPtr->txDroppedPackets++;
        return ((int32_t)ICSS_EMAC_ERR_SWITCH_INVALID_PARAM);
    }
    if(lengthOfPacket > (int32_t)ICSS_EMAC_MAXMTU)
    {    
        hostStatPtr->txDroppedPackets++;
        return ((int32_t)ICSS_EMAC_ERR_BADPACKET);
    }
    if(lengthOfPacket < ICSS_EMAC_MINMTU)
    {    
        hostStatPtr->txDroppedPackets++;
        return ((int32_t)ICSS_EMAC_ERR_BADPACKET);
    }

    if(lengthOfPacket < ETHERNET_FRAME_SIZE_60)
    {
        original_length_of_packet = (uint16_t)lengthOfPacket;
        packet_min_size_padding =1U;
        lengthOfPacket = ETHERNET_FRAME_SIZE_60;
    }
    else
    {
        packet_min_size_padding = 0;
    }

    macAddr = (uint8_t*)srcAddress;

    if(linkStatus == 0U)
    {
        hostStatPtr->txDroppedPackets++;
        return ((int32_t)ICSS_EMAC_ERR_TX_NO_LINK);
    }

    ICSS_EmacPortParams *sPort;
    sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[portNumber]);
    if(emacMode == 0U) { /*Switch Mode*/

         txQueue = &(sPort->queue[queuePriority]);
        /* Check whether Queue is busy.If yes then put the packet in the collision Queue. Set the busy_s bit because Host is always a Slave. */
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset + 4U);
        temp =  HWREG(temp_addr);
        collision_queue_selected =  (temp & 0x00000100U);  /* Check the "busy_m" bit */
        if(collision_queue_selected != 0U)
        {
           num_of_collision_occured = num_of_collision_occured +1;
           /* Queue is busy  .. put the packet in the collision Queue */
           txQueue = &(sPort->queue[ICSS_EMAC_COLQUEUE]);
            if(portNumber == ICSS_EMAC_PORT_1)
            {
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + COLLISION_STATUS_ADDR + 1U);
                col_queue_already_occupied = HWREGB(temp_addr);
            }
            else
            {
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + COLLISION_STATUS_ADDR + 2U);
                col_queue_already_occupied = HWREGB(temp_addr);
            }
            if(col_queue_already_occupied != 0)
            {
              hostStatPtr->txDroppedPackets++;
              collision_pkt_dropped ++;
              return ((int32_t)ICSS_EMAC_ERR_COLLISION_FAIL);   /*No space in collision queue */
            }
        }
        else
        {
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset + 4U);
            /* Queue is Not Busy .. Acquire the Queue by setting "busy_s" bit */
            HWREGB(temp_addr) = 1; /* Set the "busy_s" bit */
            /*Again check if host acquired the queue successfully by checking the busy_m bit */
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset + 4U);
            temp =  HWREG(temp_addr);
            collision_queue_selected =  (temp & 0x00000100U);  /* Check the "busy_m" bit */
            if(collision_queue_selected != 0U)
            {
                num_of_collision_occured = num_of_collision_occured +1;
                temp_addr = ( (((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr+ txQueue->queue_desc_offset + 4U);
                HWREGB(temp_addr) = 0; /* Clear the busy_s bit */
                /* Queue is busy  .. put the packet in the collision Queue */
                txQueue = &(sPort->queue[ICSS_EMAC_COLQUEUE]);
            }
        }
    } else { /*MAC Mode*/
        txQueue = &(sPort->queue[queuePriority]);
        /* Queue is Not Busy .. Acquire the Queue by setting "busy_s" bit */
        temp_addr = (pruSharedMem + txQueue->queue_desc_offset + 4U);
        HWREGB(temp_addr) = 1U; /* Set the "busy_s" bit */

    }
    /*  Compute the buffer descriptor ..length is from bit 18 to 28 */
    buffer_des = (((uint32_t)(lengthOfPacket)) << 18U);

    if(emacMode == 0U) { /*Switch Mode*/
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset);
        /*  Read the write pointer from Queue descriptor */
        temp =  HWREG(temp_addr);
    } else {
        temp_addr = (pruSharedMem + txQueue->queue_desc_offset);
        temp =  HWREG(temp_addr);
    }

    queue_wr_ptr = ((uint16_t)(temp >> 16));
    queue_rd_ptr = ((uint16_t)(temp & 0x0000ffffU));

    wrk_queue_wr_ptr = (((uint16_t)(lengthOfPacket)) >> 5U);  /* Divide by 32 */
    wrk_queue_wr_ptr = (wrk_queue_wr_ptr << 2);  /* Multiply by 4 ..as one descriptor represents 32 bytes and BD takes 4 bytes */
    if((((uint32_t)(lengthOfPacket)) & 0x0000001fU) != 0U)
    {
         wrk_queue_wr_ptr = wrk_queue_wr_ptr + 4U;
    }

    /* Add to get the value of new queue write pointer */
    wrk_queue_wr_ptr = wrk_queue_wr_ptr + queue_wr_ptr;
    size = txQueue->queue_size;
    /*Check if queue is full and there is an wrap around */
    if(((queue_wr_ptr + 4U) % size) == 0U)
    {
        if(queue_rd_ptr == txQueue->buffer_desc_offset) /* Since queue is not starting from 0. */
        {
            txQueue->qStat.errCount++;
            if(emacMode == 0U) { /*Switch Mode*/
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset + 4U);
                HWREGB(temp_addr) = 0;
            } else {
                temp_addr = (pruSharedMem + txQueue->queue_desc_offset + 4U);
                HWREGB(temp_addr) = 0U;
            }
            hostStatPtr->txDroppedPackets++;
            return ((int32_t)ICSS_EMAC_ERR_TX_OUT_OF_BD);   /* No space in queue */
        }
    }
    /* Check if the Queue is already full */
    if((queue_wr_ptr + 4U) == queue_rd_ptr)
    {
        txQueue->qStat.errCount++;
        if(emacMode == 0U) { /*Switch Mode*/
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr+ txQueue->queue_desc_offset + 4U);
            HWREGB(temp_addr) = 0;
        } else {
            temp_addr = (pruSharedMem + txQueue->queue_desc_offset + 4U);
            HWREGB(temp_addr) = 0U;
        }
        hostStatPtr->txDroppedPackets++;
        return ((int32_t)ICSS_EMAC_ERR_TX_OUT_OF_BD);       /* No space in queue */
    }
    /* Three cases arise between wr_ptr and rd_ptr */
    if(queue_wr_ptr == queue_rd_ptr)
    {
        /*Check for wrap around */
        if(wrk_queue_wr_ptr >=  size)
        {
            wrk_queue_wr_ptr = (wrk_queue_wr_ptr % size);
            /*Add offset as queue doesn't start from 0. */
            wrk_queue_wr_ptr = wrk_queue_wr_ptr + txQueue->buffer_desc_offset;
        }
    }
    else if(queue_wr_ptr > queue_rd_ptr)
    {
        /*Check for wrap around */
        if(wrk_queue_wr_ptr >=  size)
        {
            wrk_queue_wr_ptr = (wrk_queue_wr_ptr % size);
            wrk_queue_wr_ptr = wrk_queue_wr_ptr + txQueue->buffer_desc_offset;
            if(wrk_queue_wr_ptr >= queue_rd_ptr)
            {
                txQueue->qStat.errCount++;
                if(emacMode == 0U) { /*Switch Mode*/
                    temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset + 4U);
                    HWREGB(temp_addr) = 0;
                } else {
                    temp_addr = (pruSharedMem + txQueue->queue_desc_offset + 4U);
                    HWREGB(temp_addr) = 0U;
                }
                hostStatPtr->txDroppedPackets++;
                return ((int32_t)ICSS_EMAC_ERR_TX_OUT_OF_BD);      /* No space in queue */
            }
        }
    }
    else
    {
        if(wrk_queue_wr_ptr >= queue_rd_ptr)
        {
            txQueue->qStat.errCount++;
            if(emacMode == 0U) { /*Switch Mode*/
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset + 4U);
                HWREGB(temp_addr) = 0;
            } else {
                temp_addr = (pruSharedMem + txQueue->queue_desc_offset + 4U);
                HWREGB(temp_addr) = 0U;
            }
            hostStatPtr->txDroppedPackets++;
            return ((int32_t)ICSS_EMAC_ERR_TX_OUT_OF_BD);                          /* No space in queue */
        }
    }
    /* Compute the offset of buffer descriptor in ICSS shared RAM */
    buffer_offset_computed = txQueue->buffer_offset + ((queue_wr_ptr - txQueue->buffer_desc_offset)*8U);    /* queue_wr_ptr points to currently available free buffer */

    /* Check if queue wrap around has happened. If yes then data can't be stored sequentially. */
    if( (wrk_queue_wr_ptr < queue_wr_ptr) &&  (wrk_queue_wr_ptr != txQueue->buffer_desc_offset))
    {
        num_of_bytes = (size - queue_wr_ptr);
        num_of_bytes *= 8U;                    /* divide by 4 * 32! */
        /* check if Padding has to be done. If yes then Pad with Zero's to reach the minimum size for the ethernet frame. */
        if(packet_min_size_padding == 1U)
        {
            if( num_of_bytes <= original_length_of_packet)
            {
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed));
                memcpy((int32_t*)(temp_addr), (int32_t*) srcAddress, num_of_bytes);
            }
            else
            {
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed));
                /* Copy the valid packet data first and then Pad with zero's.  */
                memcpy((int32_t*)(temp_addr), (int32_t*) srcAddress, original_length_of_packet);
                /* Padd the remaining bytes with Zero's */
                for(i=0; i< (num_of_bytes - original_length_of_packet); i++)
                {
                    temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr+ ((uint32_t)buffer_offset_computed) + original_length_of_packet + i);
                    HWREGB(temp_addr) = 0;
                }
            }
        }
        else
        {
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed));
            memcpy((int32_t*)(temp_addr), (int32_t*) srcAddress, num_of_bytes);
        }
        new_packet_length = ((uint16_t)lengthOfPacket) - num_of_bytes;
        srcAddress = srcAddress + num_of_bytes;

        if(emacMode == 0U) { /*Switch Mode*/
            if(collision_queue_selected != 0) {
                buffer_offset_computed = buffer_offset_computed + num_of_bytes;
            } else {
                buffer_offset_computed = txQueue->buffer_offset;
            }
        } else { /*MAC Mode*/
            buffer_offset_computed = txQueue->buffer_offset;
        }

        if( packet_min_size_padding == 1u)
        {
            if(    original_length_of_packet <= num_of_bytes)
            {
                /* Pad all the remaining bytes with Zero's */
                for(i=0; i< new_packet_length; i++)
                {
                    temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed) + i);
                    HWREGB(temp_addr) = 0;
                }
            }
            else
            {
                /* Fill the frame data  */
                remaining_valid_frame_data_length = (original_length_of_packet- num_of_bytes);
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed));
                memcpy((int32_t*)(temp_addr),(int32_t*) srcAddress, remaining_valid_frame_data_length);
                /*Pad the remaining bytes with Zero's */
                for(i=0; i< (new_packet_length - remaining_valid_frame_data_length); i++)
                {
                    temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed) + remaining_valid_frame_data_length+ i);
                    HWREGB(temp_addr) = 0;
                }
            }
        }    
        else
        {
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed));
            memcpy((int32_t*)(temp_addr),(int32_t*) srcAddress, new_packet_length);
        }
    }
    else
    {
        if( packet_min_size_padding  == 1u)
        {
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed));
            memcpy((int32_t*)(temp_addr), (int32_t*)srcAddress, original_length_of_packet);
            /* Padd the remaining bytes with Zero's */
            for(i=0; i< (((uint16_t)lengthOfPacket) - original_length_of_packet); i++)
            {
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed) + original_length_of_packet + i);
                HWREGB(temp_addr) = 0;
            }
        }
        else
        {
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + ((uint32_t)buffer_offset_computed));
            memcpy((int32_t*)(temp_addr), (int32_t*)srcAddress, (uint32_t)lengthOfPacket);
        }
    }
    if(emacMode == 0U) { /*Switch Mode*/
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + ((uint32_t)queue_wr_ptr) );
        HWREG(temp_addr) = buffer_des;
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset +2U);
        /* Write new wr_ptr in the queue descriptor */
        HWREGH(temp_addr) = wrk_queue_wr_ptr;
    } else {
        temp_addr = (pruSharedMem + queue_wr_ptr );
        HWREG(temp_addr) = buffer_des;
        temp_addr = (pruSharedMem + txQueue->queue_desc_offset +2U);
        HWREGH(temp_addr) = wrk_queue_wr_ptr;
    }

    txQueue->qStat.rawCount++;

    if(emacMode == 0U) { /*Switch Mode*/
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + txQueue->queue_desc_offset + 4U);
        /* Release the Queue. Clear the "busy_s" bit .. even if collision queue was selected then below line won't have any impact. */
        HWREGB(temp_addr) = 0;
    } else {
        temp_addr = (pruSharedMem + txQueue->queue_desc_offset + 4U);
        HWREGB(temp_addr) = 0U;
    }

    if(emacMode == 0U) { /*Switch Mode*/
        /* If packet was put in collision queue then indiciate it to collision task */
        if(collision_queue_selected != 0)
        {
            if(portNumber ==ICSS_EMAC_PORT_1)
            {
                collision_status = ((uint16_t)queuePriority);
                collision_status = (collision_status << 1);
                collision_status = (collision_status | 0x0001U);
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + COLLISION_STATUS_ADDR +1U);
                HWREGB(temp_addr) = collision_status;
            }
            else
            {
                collision_status = ((uint16_t)queuePriority);
                collision_status = (collision_status << 1);
                collision_status = (collision_status | 0x0001U);
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + COLLISION_STATUS_ADDR + 2U);
                HWREGB(temp_addr) = collision_status;
            }
        }
    }

    
    if(emacMode == 0U) /*In Switch Mode both the statistics structures are in single handle.Depending on port update the corresponding structure*/
    hostStatPtr += (portNumber - 1);
    ICSS_EmacUpdateTxStats(macAddr,(uint32_t)lengthOfPacket, hostStatPtr);
    return 0;
}

/**
 *  @b Description
 *  @n
 *       API to retrieve the information about the received frame which
 *       is then used to dequeue the frame from the host queues
 *
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance.
 *  @param[out]  portNumber    Return pointer of port number where frame was received
 *  @param[out]  queueNumber   Return pointer of host queue where the received frame is queued
 *  @param[out]  pktProc       Return pointer of packet type

 *  @retval     none
 */
int32_t ICSS_EmacRxPktInfo(ICSS_EmacHandle icssEmacHandle,
                           int32_t* portNumber,
                           int32_t* queueNumber,
		                   int32_t* pktProc)

{
    uint16_t queue_rd_ptr;
    uint16_t queue_wr_ptr;
    uint32_t rd_buf_desc=0;
    uint16_t rd_packet_length;
    int32_t packet_found =0;
    Queue *qDesc;
    uint16_t shadow=0;
    uint32_t rd_buffer_l3_addr;
    uint16_t rd_buf_desc_num;
    ICSS_EmacQueueParams *rxQueue;

    uint8_t initPrioQueue = ICSS_EMAC_QUEUE1;
    uint8_t finalPrioQueue = (uint8_t)ICSS_EMAC_QUEUE4;
    uint8_t i= ICSS_EMAC_QUEUE1;

    uint8_t emacMode=0;
    uint32_t temp_addr = 0U;
    uint32_t temp_var1 = 0U;
    uint32_t temp_var2 = 0U;

    ICSS_EmacPortParams *sPort;

    switch((((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->portMask)
    {
        case ICSS_EMAC_MODE_SWITCH:
            emacMode =0;
            initPrioQueue = ICSS_EMAC_QUEUE1;
            finalPrioQueue = (uint8_t)ICSS_EMAC_QUEUE4;
            break;
        case ICSS_EMAC_MODE_MAC1:
            emacMode =1u;
            initPrioQueue = ICSS_EMAC_QUEUE1;
            finalPrioQueue= (uint8_t)ICSS_EMAC_QUEUE2;
            break;
        case ICSS_EMAC_MODE_MAC2:
            emacMode =1u;
            initPrioQueue = (uint8_t)ICSS_EMAC_QUEUE3;
            finalPrioQueue= (uint8_t)ICSS_EMAC_QUEUE4;
            break;
        default:
            break;
    }

    i=initPrioQueue;

    while((packet_found == 0) && (i <= finalPrioQueue))
    {

        if(emacMode == 0U) { /*Switch Mode*/
            temp_var1 = ((uint32_t)(i))*8U;
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + P0_QUEUE_DESC_OFFSET + temp_var1);
            qDesc = (Queue *)(temp_addr);
        } else {
            temp_var1 = ((uint32_t)(i))*8U;
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + HOST_QUEUE_DESC_OFFSET + temp_var1);
            qDesc = (Queue *)(temp_addr);
        }
        queue_wr_ptr = qDesc->wr_ptr;
        queue_rd_ptr = qDesc->rd_ptr;

        sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_0]);

        if(qDesc->overflow_cnt > 0)
        {
            sPort->queue[i].qStat.errCount += qDesc->overflow_cnt;  /* increment missed packets to error counter */
            qDesc->overflow_cnt = 0;    /* reset to zero as limited to 256 anyway */
        }
        if(queue_rd_ptr != queue_wr_ptr)
        {
            (*queueNumber) = i;
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + ((uint32_t)queue_rd_ptr));
            rd_buf_desc = HWREG(temp_addr);
            /* Take out the port number */
            (*portNumber) = (0x00030000U & rd_buf_desc) >> 16U;
            /* Get the length */
            rd_packet_length = ((uint16_t)((0x1ffc0000U & rd_buf_desc) >> 18U));
            packet_found = 1;

    	    rxQueue = &(sPort->queue[i]);

	    if(emacMode == 0U) { /*Switch Mode*/
        	/* Determine the address of the first buffer descriptor from the rd_ptr */
	        /*Check if Packet was received in collision queue or not */
	        shadow = ((uint16_t)((rd_buf_desc & 0x00004000U) >> 14U));
        	if(shadow != 0)
	        {
        	  /* Pick the data from collision buffer's */
	            rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr+ P0_COL_BUFFER_OFFSET);
        	} else  {
	            rd_buf_desc_num = (queue_rd_ptr - rxQueue->buffer_desc_offset) >> 2;
        	    temp_var1 = (((uint32_t)(rd_buf_desc_num)) * 32U);
	            temp_var2 = (rxQueue->buffer_offset);
        	    rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + temp_var1 + temp_var2);
	        }
	    } else {
        	rd_buf_desc_num = (queue_rd_ptr - rxQueue->buffer_desc_offset) >> 2;
	        temp_var1 = ((uint32_t)(rd_buf_desc_num)) * 32U;
	        temp_var2 = (rxQueue->buffer_offset);
        	rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + temp_var1 + temp_var2);
	    }
	    *pktProc = PruSwitch((char *)rd_buffer_l3_addr);
        }
        i++;
    }
    /* Received IRQ but can't find the packet in any queue */
    if(packet_found == 0)
    {
        rd_packet_length = 0;
    }
    return (int32_t)rd_packet_length;
}

/**
 *  @b Description
 *  @n
 *      Retrieves a frame from a host queue and copies it
 *           in the allocated stack buffer
 *
 *  @param[in]  rxArg defined at @ref ICSS_EmacRxArgument
 *  @param[in]  userArg custom Rx packet callback packet options only required for custom RxPacket implementations,
                default to NULL when calling ICSS_EmacRxPktGet which is default Tx Packet API
 *  @retval     Length of the frame received in number of bytes or -1 on Failure
 */
int32_t ICSS_EmacRxPktGet(ICSS_EmacRxArgument *rxArg, void* userArg)

{
    uint16_t queue_rd_ptr;
    uint16_t queue_wr_ptr;
    uint16_t rd_buf_desc_num;
    uint32_t rd_buf_desc=0;
    uint16_t rd_packet_length;
    uint32_t rd_buffer_l3_addr;
    uint16_t size =0;
    uint16_t update_rd_ptr=0;
    uint16_t rx_num_of_bytes=0;
    uint16_t new_size =0;
    ICSS_EmacQueueParams *rxQueue;
    Queue *qDesc;
    uint16_t    shadow=0;

    uint8_t* srcMacId;
    uint8_t* destMacId;

    uint16_t *typeProt;
    uint16_t  typeProt1;
    uint16_t  typeProt2;

    uint8_t emacMode=0;

    uint32_t temp_addr = 0U;
    uint32_t temp_var1 = 0U;
    uint32_t temp_var2 = 0U;

    uint8_t ret_flag = 0U;
    int32_t ret_val = 0;

    ICSS_EmacHandle icssEmacHandle = rxArg->icssEmacHandle;
    uint32_t  destAddress = rxArg->destAddress;
    uint8_t queueNumber= rxArg->queueNumber;

    
    ICSS_EmacPortParams *sPort;
    if((((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->portMask == ICSS_EMAC_MODE_SWITCH)
    {
        emacMode =0;
    }
    else
    {
        emacMode =1U;
    }

    HashTable_t *macTablePtr;
    ICSS_EmacHostStatistics_t* hostStatPtr;

    ICSS_EmacCallBackConfig* learningExcCallback;

#ifdef SWITCH_DEBUG
        genSeqOfEvents(RX_PACKET_GET);
#endif

    if(emacMode == 0U) { /*Switch Mode*/
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr+ P0_QUEUE_DESC_OFFSET + (((uint32_t)(queueNumber))*8U));
        qDesc = (Queue *)(temp_addr);
    } else {
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + HOST_QUEUE_DESC_OFFSET + (((uint32_t)(queueNumber))*8U));
        qDesc = (Queue *)(temp_addr);
    }
    queue_wr_ptr = qDesc->wr_ptr;
    queue_rd_ptr = qDesc->rd_ptr;
    sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_0]);
    if(qDesc->overflow_cnt > 0)
    {
        sPort->queue[queueNumber].qStat.errCount += qDesc->overflow_cnt;        /* increment missed packets to error counter */
        qDesc->overflow_cnt = 0;
        /* reset to zero as limited to 256 anyway */
    }
    temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + ((uint32_t)queue_rd_ptr));
    rd_buf_desc = HWREG(temp_addr);
    rxQueue = &(sPort->queue[queueNumber]);

    if(emacMode == 0U) { /*Switch Mode*/
        /* Determine the address of the first buffer descriptor from the rd_ptr */
        /*Check if Packet was received in collision queue or not */
        shadow = ((uint16_t)((rd_buf_desc & 0x00004000U) >> 14U));
        if(shadow != 0)
        {
          /* Pick the data from collision buffer's */
            rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr+ P0_COL_BUFFER_OFFSET);
        } else  {
            rd_buf_desc_num = (queue_rd_ptr - rxQueue->buffer_desc_offset) >> 2;
            temp_var1 = (((uint32_t)(rd_buf_desc_num)) * 32U);
            temp_var2 = (rxQueue->buffer_offset);
            rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + temp_var1 + temp_var2);
        }
    } else {
        rd_buf_desc_num = (queue_rd_ptr - rxQueue->buffer_desc_offset) >> 2;
        temp_var1 = ((uint32_t)(rd_buf_desc_num)) * 32U;
        temp_var2 = (rxQueue->buffer_offset);
        rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + temp_var1 + temp_var2);
    }
    /* Take out the port number - it may have changed */
    rxArg->port = (0x00030000U & rd_buf_desc)>>16;

    temp_addr = (rd_buffer_l3_addr + 6U);
    srcMacId = (uint8_t*)(temp_addr);

    destMacId = (uint8_t*)rd_buffer_l3_addr;


    rd_packet_length = ((uint16_t)((0x1ffc0000U & rd_buf_desc) >> 18));

    size = (rd_packet_length >>2);
    if( (rd_packet_length & 0x00000003U) != 0U )
    {
        size = size + 1u;
    }

    /*Compute number of buffer desc required & update rd_ptr in queue */
    update_rd_ptr = ((rd_packet_length >> 5U)*4U) + queue_rd_ptr;
    if( (rd_packet_length & 0x0000001fU) != 0U) /* checks multiple of 32 else need to increment by 4 */
    {
        update_rd_ptr += 4U;
    }
    /*Check for wrap around */
    if(update_rd_ptr >= rxQueue->queue_size)
    {
        update_rd_ptr = update_rd_ptr - (rxQueue->queue_size - rxQueue->buffer_desc_offset);
    }
    if(rd_packet_length <= ICSS_EMAC_MAXMTU)        /* make sure we do not have too big packets */
    {

        if((((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->learningEn) { /*Switch Mode*/

            learningExcCallback = (((ICSS_EmacObject*)icssEmacHandle->object)->callBackHandle)->learningExCallBack;
            macTablePtr = (HashTable_t*)(((ICSS_EmacObject*)icssEmacHandle->object)->macTablePtr);
            updateHashTable(srcMacId, rxArg->port, macTablePtr,learningExcCallback);

        }

        /* Copy the data from switch buffers to DDR */
        if( (update_rd_ptr < queue_rd_ptr) && (update_rd_ptr != rxQueue->buffer_desc_offset))
        {
            typeProt = (uint16_t*)rd_buffer_l3_addr + 6;
            typeProt1 = ((uint16_t)((*typeProt) << 8U));
            typeProt2 = ((uint16_t)((*typeProt) >> 8U));
            typeProt1 = typeProt1 | typeProt2;
            rx_num_of_bytes = (rxQueue->queue_size - queue_rd_ptr);
            rx_num_of_bytes = (rx_num_of_bytes >> 2);
            rx_num_of_bytes = (rx_num_of_bytes << 5);

            memcpy((int32_t*)destAddress, (int32_t*)rd_buffer_l3_addr, rx_num_of_bytes);
              destAddress = destAddress + rx_num_of_bytes;
              new_size = rd_packet_length - rx_num_of_bytes;

              if(emacMode == 0U) { /*Switch Mode*/
                  if(shadow != 0) {
                      rd_buffer_l3_addr = rd_buffer_l3_addr + rx_num_of_bytes;
                  } else {
                      rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + rxQueue->buffer_offset);
                  }
              } else {
                  rd_buffer_l3_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->l3OcmcBaseAddr + rxQueue->buffer_offset);
              }

              memcpy((int32_t*)destAddress, (int32_t*)rd_buffer_l3_addr, new_size);
        }
        else
        {

            memcpy((int32_t*)destAddress, (int32_t*)rd_buffer_l3_addr, rd_packet_length);
            typeProt = (uint16_t*)destAddress + 6;
            typeProt1 = ((uint16_t)((*typeProt) << 8U));
            typeProt2 = ((uint16_t)((*typeProt) >> 8U));
            typeProt1 = typeProt1 | typeProt2;
        }
    }
    else  /* wrong packet size (exceeds ICSS_EMAC_MAXMTU)*/
    {
        rxQueue->qStat.errCount++;
        ret_flag = 1U;
        ret_val = -1;
    }

    if(ret_flag == 0U)
    {
        if(emacMode == 0U) { /*Switch Mode*/
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + P0_QUEUE_DESC_OFFSET + (((uint32_t)(queueNumber))*8U));
            /* Write back to queue */
            HWREGH(temp_addr) = update_rd_ptr;
             /* Check if Host needs to change the wr_ptr for collision queue as well */
            if(shadow != 0)
            {
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + P0_COL_QUEUE_DESC_OFFSET);
                Queue *qDescCol = (Queue *)(temp_addr);
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + P0_COL_QUEUE_DESC_OFFSET +2U);
                /*Write back to collision queue desc */
                HWREGH(temp_addr) = qDescCol->rd_ptr;
                
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + COLLISION_STATUS_ADDR +3U);
                HWREGH(temp_addr) = 0;

            }
        } else {
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + HOST_QUEUE_DESC_OFFSET + (((uint32_t)(queueNumber))*8U));
            HWREGH(temp_addr) = update_rd_ptr;
        }
        rxQueue->qStat.rawCount++;

        rxArg->more = 0;
        if(emacMode == 0U) { /*Switch Mode*/
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + P0_QUEUE_DESC_OFFSET + (((uint32_t)(queueNumber))*8U));
            /* get new pointer data in case new packets received in meantime - experimental.. */
            qDesc = (Queue *)(temp_addr);
        } else {
            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + HOST_QUEUE_DESC_OFFSET + (((uint32_t)(queueNumber))*8U));
            qDesc = (Queue *)(temp_addr);
        }
        queue_wr_ptr = qDesc->wr_ptr;

        if(update_rd_ptr != queue_wr_ptr)
        {
            rxArg->more = 1;
        }

        hostStatPtr = (ICSS_EmacHostStatistics_t*)((((ICSS_EmacObject*)icssEmacHandle->object)->hostStat));
        if(emacMode == 0U) /*In Switch Mode both the statistics structures are in single handle.Depending on port update the corresponding structure*/
            hostStatPtr += (rxArg->port - 1);
        ICSS_EmacUpdateRxStats(destMacId,rd_packet_length, typeProt1, hostStatPtr);

        ret_val = (int32_t)rd_packet_length;
    }
    return (ret_val);
}

/**
 *  @b Description
 *  @n
 *      Finds the maximum fill level of the queue in terms of 32 byte blocks.
        For example, if there was only one 64 byte packet received when this
        API is called then it would return value of 2.
        It also returns number of times queue has overflown.
 *
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance.
 *  @param[in]  portNumber    Port on which queue resides. Valid values are:
                              0 == PORT0, 1 == PORT1, 2 == PORT2
 *  @param[in]  queuePriority   Priority of the queue or queue number whose fill level has to be found
 *  @param[in]  queueType   Rx/Tx Queue
 *  @param[out]  queueOverflowCount    Number of times queue has overflown

 *  @retval     The maximum fill level of the queue in terms of 32 byte blocks or
 *              <0 if there was an error in the input parameters
 */
int32_t ICSS_EmacGetMaxQueueFillLevel(ICSS_EmacHandle icssEmacHandle,
                                      int32_t portNumber, 
                                      int32_t queuePriority, 
                                      uint8_t queueType,
                                      int32_t* queueOverflowCount)
{
    int32_t queueMaxFillLevel =0;
    uint32_t temp_addr = 0U;
    ICSS_EmacPortParams *sPort;

    int32_t ret_val = 0;

    if((portNumber != ICSS_EMAC_PORT_0) && 
       (portNumber != ICSS_EMAC_PORT_1) &&
       (portNumber != ICSS_EMAC_PORT_2))
    {    
        ret_val = ((int32_t)ICSS_EMAC_SWITCH_INVALID_PORT);
    }
    else
    {
       sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[portNumber]);
        if((queuePriority < ICSS_EMAC_QUEUE1) || (queuePriority > ICSS_EMAC_QUEUE4))
        {
            ret_val = ((int32_t)ICSS_EMAC_ERR_SWITCH_INVALID_PARAM);
        }
        else
        {
            ICSS_EmacQueueParams *queue_local = &(sPort->queue[queuePriority]);

            /*Read the max fill level for the queue */
            if(ICSS_EMAC_MODE_SWITCH == (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->portMask) { /*Switch Mode*/
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + queue_local->queue_desc_offset + Q_MAX_FILL_LEVEL_OFFSET);
                queueMaxFillLevel =  HWREGB(temp_addr);
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + queue_local->queue_desc_offset + Q_OVERFLOW_CNT_OFFSET);
                *queueOverflowCount = HWREGB(temp_addr);
            } else {
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr + queue_local->queue_desc_offset + Q_MAX_FILL_LEVEL_OFFSET);
                queueMaxFillLevel =  HWREGB(temp_addr);
                temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->sharedDataRamBaseAddr+ queue_local->queue_desc_offset + Q_OVERFLOW_CNT_OFFSET);
                *queueOverflowCount = HWREGB(temp_addr);
            }
            ret_val = queueMaxFillLevel;
        }
    }
    return ret_val;
}
/**
 *  \name ClearStatistics
 *  @brief  Function to clear queue statistics
 *
 *  @param none
 *  @retval none
 */
void ClearStatistics(ICSS_EmacHandle icssEmacHandle); /* for misra warning*/
void ClearStatistics(ICSS_EmacHandle icssEmacHandle)
{
    ICSS_EmacPortParams *sPort;

    int32_t i, j;
    for (j=0; j<3; j++)
    {
        sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[j]);
        sPort->errCount = 0;
        sPort->rawCount = 0;
        for (i=0; i<ICSS_EMAC_NUMQUEUES; i++)
        {
            sPort->queue[i].qStat.errCount = 0;
            sPort->queue[i].qStat.rawCount = 0;
        }
    }
}

void ICSS_EmacHostInit(ICSS_EmacHandle icssEmacHandle)
{
    ICSS_EmacPortParams *sPort;

    /* Initialize port 0*/
    sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_0]);
    sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P0_Q1_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P0_Q1_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = HOST_QUEUE_DESC_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (HOST_QUEUE_1_SIZE << 2) + P0_Q1_BD_OFFSET;        /* really the end of Queue */

    sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P0_Q2_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P0_Q2_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = HOST_QUEUE_DESC_OFFSET + 8U;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (HOST_QUEUE_2_SIZE << 2) + P0_Q2_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P0_Q3_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P0_Q3_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = HOST_QUEUE_DESC_OFFSET + 16U;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (HOST_QUEUE_3_SIZE << 2) + P0_Q3_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P0_Q4_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P0_Q4_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = HOST_QUEUE_DESC_OFFSET + 24U;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (HOST_QUEUE_4_SIZE << 2) + P0_Q4_BD_OFFSET;

}

void ICSS_EmacMACInit(ICSS_EmacHandle icssEmacHandle, uint8_t portNum)
{
    ICSS_EmacPortParams *sPort;

    if(((uint8_t)(ICSS_EMAC_PORT_1)) == portNum) {
        sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_1]);
       /* Initialize port 1*/
        sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P1_Q1_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P1_Q1_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P1_Q1_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P1_Q2_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P1_Q2_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 8U;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P1_Q2_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P1_Q3_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P1_Q3_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 16U;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P1_Q3_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P1_Q4_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P1_Q4_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 24U;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P1_Q4_BD_OFFSET;
    }

    if(((uint8_t)(ICSS_EMAC_PORT_2)) == portNum) {
        /* Initialize port 2*/
        sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_2]);
       sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P2_Q1_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P2_Q1_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P2_Q1_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P2_Q2_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P2_Q2_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 8U;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P2_Q2_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P2_Q3_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P2_Q3_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 16U;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P2_Q3_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P2_Q4_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P2_Q4_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 24U;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P2_Q4_BD_OFFSET;
    }

}

int32_t ICSS_EmacPortInit(ICSS_EmacHandle icssEmacHandle); /* for misra warning*/
int32_t ICSS_EmacPortInit(ICSS_EmacHandle icssEmacHandle)
{
    ICSS_EmacPortParams *sPort;
    /* Clear counters */
    ClearStatistics(icssEmacHandle);
    /* Initialize port 0*/
    sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_0]);
    sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P0_Q1_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P0_Q1_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = P0_QUEUE_DESC_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (HOST_QUEUE_1_SIZE << 2) + P0_Q1_BD_OFFSET;        /* really the end of Queue */

    sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P0_Q2_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P0_Q2_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = P0_QUEUE_DESC_OFFSET + 8U;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (HOST_QUEUE_2_SIZE << 2) + P0_Q2_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P0_Q3_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P0_Q3_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = P0_QUEUE_DESC_OFFSET + 16U;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (HOST_QUEUE_3_SIZE << 2) + P0_Q3_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P0_Q4_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P0_Q4_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = P0_QUEUE_DESC_OFFSET + 24U;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (HOST_QUEUE_4_SIZE << 2) + P0_Q4_BD_OFFSET;

    /* Initialize port 1*/
    sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_1]);
   sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P1_Q1_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P1_Q1_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = P1_QUEUE_DESC_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P1_Q1_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P1_Q2_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P1_Q2_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = P1_QUEUE_DESC_OFFSET + 8U;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P1_Q2_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P1_Q3_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P1_Q3_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = P1_QUEUE_DESC_OFFSET +16U;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P1_Q3_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P1_Q4_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P1_Q4_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = P1_QUEUE_DESC_OFFSET +24U;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P1_Q4_BD_OFFSET;
    
    /*Collision Queue */
    sPort->queue[ICSS_EMAC_COLQUEUE].buffer_offset           = P1_COL_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_COLQUEUE].buffer_desc_offset = P1_COL_BD_OFFSET;
    sPort->queue[ICSS_EMAC_COLQUEUE].queue_desc_offset  = P1_COL_QUEUE_DESC_OFFSET;
    sPort->queue[ICSS_EMAC_COLQUEUE].queue_size         = (COLLISION_QUEUE_SIZE << 2) + P1_COL_BD_OFFSET;

    /* Initialize port 2*/
    sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_2]);
    sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P2_Q1_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P2_Q1_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = P2_QUEUE_DESC_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P2_Q1_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P2_Q2_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P2_Q2_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = P2_QUEUE_DESC_OFFSET + 8U;
    sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P2_Q2_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P2_Q3_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P2_Q3_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = P2_QUEUE_DESC_OFFSET +16U;
    sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P2_Q3_BD_OFFSET;

    sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P2_Q4_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P2_Q4_BD_OFFSET;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = P2_QUEUE_DESC_OFFSET +24U;
    sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P2_Q4_BD_OFFSET;
    
    /*Collision Queue */
    sPort->queue[ICSS_EMAC_COLQUEUE].buffer_offset           = P2_COL_BUFFER_OFFSET;
    sPort->queue[ICSS_EMAC_COLQUEUE].buffer_desc_offset = P2_COL_BD_OFFSET;
    sPort->queue[ICSS_EMAC_COLQUEUE].queue_desc_offset  = P2_COL_QUEUE_DESC_OFFSET;
    sPort->queue[ICSS_EMAC_COLQUEUE].queue_size         = (COLLISION_QUEUE_SIZE << 2) + P2_COL_BD_OFFSET;

    return 0;
}


/**
*  @internal
*  @brief Function to re-initialize all Port Queue params
*
*  @param portNumber number of the port to flush
*  @retval None
*/
void macFlush(ICSS_EmacHandle icssEmacHandle, Int32 portNumber); /* for misra warning*/
void macFlush(ICSS_EmacHandle icssEmacHandle, Int32 portNumber)
{
    ICSS_EmacPortParams *sPort;

    switch(portNumber) {
        case ICSS_EMAC_PORT_1:
        sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_1]);
        /* Initialize port 1*/
        sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P1_Q1_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P1_Q1_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P1_Q1_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P1_Q2_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P1_Q2_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 8U;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P1_Q2_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P1_Q3_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P1_Q3_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET +16U;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P1_Q3_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P1_Q4_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P1_Q4_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET +24U;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P1_Q4_BD_OFFSET;
        break;

    case ICSS_EMAC_PORT_2:
        /* Initialize port 2*/
        sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_2]);
        sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset       = P2_Q1_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P2_Q1_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P2_Q1_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset       = P2_Q2_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P2_Q2_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET + 8U;
        sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P2_Q2_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset       = P2_Q3_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P2_Q3_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET +16U;
        sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P2_Q3_BD_OFFSET;

        sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset       = P2_Q4_BUFFER_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P2_Q4_BD_OFFSET;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = PORT_QUEUE_DESC_OFFSET +24U;
        sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P2_Q4_BD_OFFSET;
        break;

    default:
        break;
    }

}

/**
 *  @b Description
 *  @n
 *      API Function to re-initialize all Port Queue params
 * 
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance
 *  @param[in]  portNumber number of the port to flush
 *  @retval None
 */
void ICSS_EmacPortFlush(ICSS_EmacHandle icssEmacHandle, int32_t portNumber)
{
  ICSS_EmacPortParams *sPort;

  switch ( portNumber )
  {
    case ICSS_EMAC_PORT_1:
      sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_1]);

      sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset      = P1_Q1_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P1_Q1_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = P1_QUEUE_DESC_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P1_Q1_BD_OFFSET;

      sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset      = P1_Q2_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P1_Q2_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = P1_QUEUE_DESC_OFFSET + 8U;
      sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P1_Q2_BD_OFFSET;

      sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset      = P1_Q3_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P1_Q3_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = P1_QUEUE_DESC_OFFSET +16U;
      sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P1_Q3_BD_OFFSET;

      sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset      = P1_Q4_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P1_Q4_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = P1_QUEUE_DESC_OFFSET +24U;
      sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P1_Q4_BD_OFFSET;

      sPort->queue[ICSS_EMAC_COLQUEUE].buffer_offset      = P1_COL_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_COLQUEUE].buffer_desc_offset = P1_COL_BD_OFFSET;
      sPort->queue[ICSS_EMAC_COLQUEUE].queue_desc_offset  = P1_COL_QUEUE_DESC_OFFSET;
      sPort->queue[ICSS_EMAC_COLQUEUE].queue_size         = (COLLISION_QUEUE_SIZE << 2) + P1_COL_BD_OFFSET;

      break;

    case ICSS_EMAC_PORT_2:
      sPort = &(((ICSS_EmacObject*)icssEmacHandle->object)->switchPort[ICSS_EMAC_PORT_2]);

      sPort->queue[ICSS_EMAC_QUEUE1].buffer_offset      = P2_Q1_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE1].buffer_desc_offset = P2_Q1_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE1].queue_desc_offset  = P2_QUEUE_DESC_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE1].queue_size         = (QUEUE_1_SIZE << 2) + P2_Q1_BD_OFFSET;

      sPort->queue[ICSS_EMAC_QUEUE2].buffer_offset      = P2_Q2_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE2].buffer_desc_offset = P2_Q2_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE2].queue_desc_offset  = P2_QUEUE_DESC_OFFSET + 8U;
      sPort->queue[ICSS_EMAC_QUEUE2].queue_size         = (QUEUE_2_SIZE << 2) + P2_Q2_BD_OFFSET;

      sPort->queue[ICSS_EMAC_QUEUE3].buffer_offset      = P2_Q3_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE3].buffer_desc_offset = P2_Q3_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE3].queue_desc_offset  = P2_QUEUE_DESC_OFFSET +16U;
      sPort->queue[ICSS_EMAC_QUEUE3].queue_size         = (QUEUE_3_SIZE << 2) + P2_Q3_BD_OFFSET;

      sPort->queue[ICSS_EMAC_QUEUE4].buffer_offset      = P2_Q4_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE4].buffer_desc_offset = P2_Q4_BD_OFFSET;
      sPort->queue[ICSS_EMAC_QUEUE4].queue_desc_offset  = P2_QUEUE_DESC_OFFSET +24U;
      sPort->queue[ICSS_EMAC_QUEUE4].queue_size         = (QUEUE_4_SIZE << 2) + P2_Q4_BD_OFFSET;

      sPort->queue[ICSS_EMAC_COLQUEUE].buffer_offset      = P2_COL_BUFFER_OFFSET;
      sPort->queue[ICSS_EMAC_COLQUEUE].buffer_desc_offset = P2_COL_BD_OFFSET;
      sPort->queue[ICSS_EMAC_COLQUEUE].queue_desc_offset  = P2_COL_QUEUE_DESC_OFFSET;
      sPort->queue[ICSS_EMAC_COLQUEUE].queue_size         = (COLLISION_QUEUE_SIZE << 2) + P2_COL_BD_OFFSET;

      break;

    default:
      break;
  }
}
/**
 *  @b Description
 *  @n
 *      API to register the hardware interrupt receive packet callback function
 *
 *  @param[in]  hwIntRx    hardware interrupt receive packet callback function
 *  @retval     none
 */
void ICSS_EmacRegisterHwIntRx (ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack hwIntRx)
{
    ((ICSS_EmacObject*)icssEmacHandle->object)->icssEmacHwIntRx = hwIntRx;
}

/**
 *  @b Description
 *  @n
 *      API to register the hardware interrupt for Transmit packet complete by PRU-ICSS firmware
 *
 *  @param[in]  hwIntRx    hardware interrupt transmit packet complete callback function
 *  @retval     none
 */
void ICSS_EmacRegisterHwIntTx(ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack hwIntTx)
{
    ((ICSS_EmacObject*)icssEmacHandle->object)->icssEmacHwIntTx = hwIntTx;
}
int8_t ICSS_EmacOSInit(ICSS_EmacHandle icssEmacHandle)
{
    int8_t ret_val = 0;

#ifdef ENABLE_TIMER_SUPPORT
    uint8_t pacingMode;
    TimerP_Params timerParams;
#endif
    SemaphoreP_Params semParams;

    ICSS_EMAC_osalSemParamsInit(&semParams);
    semParams.mode = SemaphoreP_Mode_BINARY;
    semParams.name= "rxSemaphore";

    ((ICSS_EmacObject*)icssEmacHandle->object)->rxSemaphoreHandle =  ICSS_EMAC_osalCreateBlockingLock(0,&semParams);
    if(((ICSS_EmacObject*)icssEmacHandle->object)->rxSemaphoreHandle==NULL)
    {
        ret_val = -1;
    }
    else
    {
        ICSS_EMAC_osalSemParamsInit(&semParams);
        semParams.mode = SemaphoreP_Mode_BINARY;
        semParams.name= "txSemaphore";
        ((ICSS_EmacObject*)icssEmacHandle->object)->txSemaphoreHandle =  ICSS_EMAC_osalCreateBlockingLock(0,&semParams);
        if(((ICSS_EmacObject*)icssEmacHandle->object)->txSemaphoreHandle==NULL)
        {
            ret_val = -1;
        }
#ifdef ENABLE_TIMER_SUPPORT
        else
        {
            pacingMode = (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->ICSS_EmacIntrPacingMode;
            if(pacingMode == ICSS_EMAC_INTR_PACING_MODE2)
            {
                ICSS_EMAC_osalTimerParamsInit(&timerParams);

                timerParams.period = DEFAULT_PACING_TIMER_VAL;
                timerParams.arg = (void*)icssEmacHandle;
                timerParams.freqHi = 0;
                timerParams.freqLo = DMTIMER_SRC_CLK_FREQ;

                /*Create Interrupt Pacing Timer*/
                ICSS_EMAC_osalTimerCreate(DMTIMER4_ID, (TimerP_Fxn)ICSS_EmacInterruptPacingISR, &timerParams);

                if (pacingTimerHandle == NULL) {
                    ret_val = -1;
                }
            }
        }
#endif
    }
    return ret_val;
}
/**
* @internal
* @brief Function to delete Rx task (to receive packet)
*
* @param none
*
* @retval Success -   0
*         Error   -   <0
*/
int8_t ICSS_EmacOSDeInit(ICSS_EmacHandle icssEmacHandle)
{
    ICSS_EMAC_osalDeleteBlockingLock(((ICSS_EmacObject*)icssEmacHandle->object)->rxSemaphoreHandle);
    return 0;
}

void ICSS_EmacAddProtocolToList(uint16_t protocolType);   /* for misra warning*/
void ICSS_EmacAddProtocolToList(uint16_t protocolType)
{
    if(MAX_NUM_PROTOCOL_IMPLEMENTED > numImplementedProtocols) {
        protocol_impl[numImplementedProtocols] =  protocolType;
        numImplementedProtocols++;
    }
}

void ICSS_EmacUpdatePhyStatus(uint8_t portNum,ICSS_EmacHandle icssEmacHandle);  /* for misra warning*/
void ICSS_EmacUpdatePhyStatus(uint8_t portNum,ICSS_EmacHandle icssEmacHandle) 
{

    volatile uint8_t *portStatusPtr=NULL;
    volatile uint8_t portStatus = 0;
    uint8_t linkStatus=0;
    volatile uint32_t *phySpeedStatusPtr=NULL;
    uint8_t index=0;
    uint16_t phyStat=0;
    uint8_t duplexity=1U;
    uint16_t regStatus=0;

    uint32_t temp_addr = 0U;

    if(ICSS_EMAC_MODE_SWITCH == (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->portMask)
    {
        index= portNum-1U;
    }
    else
    {
        index= 0;
    }

    linkStatus = ((ICSS_EmacObject*)icssEmacHandle->object)->linkStatus[index];

    if(portNum == ((uint8_t)(ICSS_EMAC_PORT_1))) {
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam0BaseAddr + PHY_SPEED_OFFSET);
        phySpeedStatusPtr = (uint32_t*)(temp_addr);
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam0BaseAddr + PORT_STATUS_OFFSET);
        portStatusPtr = (uint8_t*)(temp_addr);
    }

    if(portNum == ((uint8_t)(ICSS_EMAC_PORT_2))) {
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + PHY_SPEED_OFFSET);
        phySpeedStatusPtr = (uint32_t*)(temp_addr);
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->dataRam1BaseAddr + PORT_STATUS_OFFSET);
        portStatusPtr = (uint8_t*)(temp_addr);
    }

    if(linkStatus)
    {
            CSL_MDIO_phyRegRead((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->prussMiiMdioRegs,
                                               (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->phyAddr[index], TLKPHY_PHYSTS_REG, &regStatus);
            if (regStatus & TLK_SPEED_STATUS) /*Speed is 10*/
            {
                if (regStatus & TLK_DUPLEX_STATUS)
                {
                    phyStat = PHY_CONFIG_10FD;
                }
                else
                {
                    phyStat =  PHY_CONFIG_10HD;
                }
            }
            else/*Speed is 100*/
            {
                if (regStatus & TLK_DUPLEX_STATUS)
                {
                    phyStat =  PHY_CONFIG_100FD;
                }
                else
                {
                    phyStat = PHY_CONFIG_100HD;
                }
        }

        if(phySpeedStatusPtr != NULL)
        {
            switch(phyStat)
            {
                case PHY_CONFIG_100FD:
                    *(phySpeedStatusPtr) = Hundread_Mbps;
                    duplexity=1u;
                    break;
                case PHY_CONFIG_100HD:
                    *(phySpeedStatusPtr) = Hundread_Mbps;
                    duplexity=0;
                    break;
                case PHY_CONFIG_10FD:
                    *(phySpeedStatusPtr) = Ten_Mbps;
                    duplexity=1u;
                    break;
                case PHY_CONFIG_10HD:
                    *(phySpeedStatusPtr) = Ten_Mbps;
                    duplexity=0;
                    break;
                default:
                    *(phySpeedStatusPtr) = Hundread_Mbps;
                    duplexity=1u;    
                    break;
            }
        }
        if((((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->halfDuplexEnable)
        {
            /*set flags for HD*/
            if(duplexity == 0U) {
                portStatus |= PORT_IS_HD_MASK;
            } else
            {
                portStatus &= ~(PORT_IS_HD_MASK);
            }
        }
        /*Set Link Up Flag*/
        portStatus |= PORT_LINK_MASK;

} 
    else
    {
        /*Clear Link Up Flag*/
        portStatus &= ~(PORT_LINK_MASK);
    }

    if(portStatusPtr != NULL)
    {
        /*write back*/
        *(portStatusPtr) = portStatus;
    }
}
/**
* @brief Link change status interrupt for Port 0 and Port 1
*         calls a user callback if defined to provide link info to stack
*
* @param arg
*
* @retval none
*/
void ICSS_EmacLinkISR(void* arg) {

    volatile uint32_t linkStatus;
    volatile uint32_t *intStatusPtr;
    ICSSEMAC_IoctlCmd ioctlParams;
    uint8_t ioctlvalue = 0;
    ICSS_EmacHandle icssEmacHandle = (ICSS_EmacHandle)arg;

    uint32_t temp_addr = 0U;

    temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->prussIntcRegs + CSL_ICSSINTC_SECR1);
    /*Find out which port it is*/
    intStatusPtr = (uint32_t*)(temp_addr);

    if(LINK0_PRU_EVT_MASK & *intStatusPtr) { /**Link 0 Port event*/
        linkStatus = ICSS_EmacPhyLinkStatusGet((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->prussMiiMdioRegs, 
                                                                 (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->phyAddr[0],
                                                                 100U);
        ((ICSS_EmacObject*)icssEmacHandle->object)->linkStatus[0]=(uint8_t)linkStatus;

        /*Update flags in memory*/
        ICSS_EmacUpdatePhyStatus((uint8_t)ICSS_EMAC_PORT_1, icssEmacHandle);

        if(linkStatus) {
            ioctlvalue = ICSS_EMAC_IOCTL_PORT_CTRL_ENABLE;
            ioctlParams.ioctlVal = &ioctlvalue;
            ICSS_EmacIoctl(icssEmacHandle, ICSS_EMAC_IOCTL_PORT_CTRL, (uint8_t)ICSS_EMAC_PORT_1, (void*)&ioctlParams);
        } else {
            ioctlvalue = ICSS_EMAC_IOCTL_PORT_CTRL_DISABLE;
            ioctlParams.ioctlVal = &ioctlvalue;
            ICSS_EmacIoctl(icssEmacHandle, ICSS_EMAC_IOCTL_PORT_CTRL, (uint8_t)ICSS_EMAC_PORT_1, (void*)&ioctlParams);
        }

        /*Protocol specific processing*/
        if(((ICSS_EmacObject*)icssEmacHandle->object)->port0ISRCall != NULL) {
           ((ICSS_EmacObject*)icssEmacHandle->object)->port0ISRCall((void *)linkStatus,((ICSS_EmacObject*)icssEmacHandle->object)->port0ISRUser);
        }
        
        temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->prussMiiMdioRegs + ICSS_MDIO_LINKINT_RAW_MASK_OFFSET);
        /*clear interrupt in MDIO*/
        HWREG(temp_addr) = 0x01;

        /*clear PRU-ICSS INTC interrupt*/
        *intStatusPtr = LINK0_PRU_EVT_MASK;
    } else {
        if(LINK1_PRU_EVT_MASK & *intStatusPtr) { /**Link 1 Port event*/
            if(ICSS_EMAC_MODE_SWITCH != ((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg->portMask) {
                linkStatus = ICSS_EmacPhyLinkStatusGet((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->prussMiiMdioRegs,
                                                                    (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->phyAddr[0],
                                                                     100U);
                ((ICSS_EmacObject*)icssEmacHandle->object)->linkStatus[0]=(uint8_t)linkStatus;
            } else {
                linkStatus = ICSS_EmacPhyLinkStatusGet((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->prussMiiMdioRegs,
                                                                   (((ICSS_EmacObject*)icssEmacHandle->object)->emacInitcfg)->phyAddr[1],
                                                                                100U);
                ((ICSS_EmacObject*)icssEmacHandle->object)->linkStatus[ICSS_EMAC_PORT_2-1]=(uint8_t)linkStatus;
            }


            /*Update flags in memory*/
            ICSS_EmacUpdatePhyStatus((uint8_t)ICSS_EMAC_PORT_2, icssEmacHandle);

            if(linkStatus) {
                ioctlvalue = ICSS_EMAC_IOCTL_PORT_CTRL_ENABLE;
                ioctlParams.ioctlVal = &ioctlvalue;
                ICSS_EmacIoctl(icssEmacHandle, ICSS_EMAC_IOCTL_PORT_CTRL, (uint8_t)ICSS_EMAC_PORT_2, (void*)&ioctlParams);
            } else {
                ioctlvalue = ICSS_EMAC_IOCTL_PORT_CTRL_DISABLE;
                ioctlParams.ioctlVal = &ioctlvalue;
                ICSS_EmacIoctl(icssEmacHandle, ICSS_EMAC_IOCTL_PORT_CTRL, (uint8_t)ICSS_EMAC_PORT_2, (void*)&ioctlParams);
            }

            if(((ICSS_EmacObject*)icssEmacHandle->object)->port1ISRCall != NULL) {
               ((ICSS_EmacObject*)icssEmacHandle->object)->port1ISRCall((void *)linkStatus,((ICSS_EmacObject*)icssEmacHandle->object)->port1ISRUser);
            }

            temp_addr = ((((ICSS_EmacHwAttrs*)icssEmacHandle->hwAttrs)->emacBaseAddrCfg)->prussMiiMdioRegs + ICSS_MDIO_LINKINT_RAW_MASK_OFFSET);
            /*clear interrupt in MDIO*/
            HWREG(temp_addr) = 0x02;

            /*clear PRU-ICSS INTC interrupt*/
            *intStatusPtr = LINK1_PRU_EVT_MASK;
        }
    }
}

/**
* @brief Callback function to process protocol specific handler for link status ISR for Port 0
*
* @param callBack    Callback function pointer
* @param userArg    user specific parameter
*
* @retval none
*/
void ICSS_EmacRegisterPort0ISRCallback(ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack callBack, void *userArg)
{
   ((ICSS_EmacObject*)icssEmacHandle->object)->port0ISRCall = callBack;
   ((ICSS_EmacObject*)icssEmacHandle->object)->port0ISRUser = userArg;
}

/**
* @brief Callback function to process protocol specific handler for link status ISR for Port 1
*
* @param callBack    Callback function pointer
* @param userArg    user specific parameter
*
* @retval none
*/
void ICSS_EmacRegisterPort1ISRCallback(ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack callBack, void *userArg)
{
   ((ICSS_EmacObject*)icssEmacHandle->object)->port1ISRCall = callBack;
   ((ICSS_EmacObject*)icssEmacHandle->object)->port1ISRUser = userArg;
}



/**
* @brief Function to status of Phy Link
*
* @param mdioBaseAddr       mdio subsystem base address
* @param phyAddr            physical address
* @param retries            retry count
*
* @retval 1 if phy link up, 0 phy link down
*/
uint32_t ICSS_EmacPhyLinkStatusGet(uint32_t mdioBaseAddr,
                              uint32_t phyAddr,
                              volatile uint32_t retries)
{
    volatile uint16_t linkStatus;
    uint32_t ret_val = FALSE_VAL;
    retries++;
    while (retries)
    {
        /* First read the BSR of the PHY */
        CSL_MDIO_phyRegRead(mdioBaseAddr, phyAddr, PHY_BSR, (uint16_t*)&linkStatus);

        if(linkStatus & PHY_LINK_STATUS)
        {
            ret_val = TRUE_VAL;
            break;
        }

        retries--;
    }

    return ret_val;
}
/**
 *  \name ICSS_EMacOsTxTaskFnc
 *  @brief
 *      Function which pends on Tx semaphore.Invokes registered Tx Callback function
 *
 *  @param a0 arg 1
 *  @param a1 arg 2
 *
 *  @retval none
 *
 */
void ICSS_EMacOsTxTaskFnc(uint32_t a0, uint32_t a1)
{
    ICSS_EmacHandle icssEmacHandle;
    icssEmacHandle = (ICSS_EmacHandle)a0;

    while(1)
    {
        ICSS_EMAC_osalPendLock(((ICSS_EmacObject*)icssEmacHandle->object)->txSemaphoreHandle, SemaphoreP_WAIT_FOREVER);
        ((ICSS_EmacObject*)icssEmacHandle->object)->icssEmacHwIntTx(icssEmacHandle,NULL);
    }
}
