#
#  ======== readme.txt ========
#
This design demonstrates packet switching and filtering logic implemented in the
M4 core of AM572x based upon the Ethertype, MAC address and Application ID 
(APPID) of GOOSE packets received from the PRU-ICSS. Packets are filtered and 
routed to destinations in order to allow the time-critical events defined in 
substation communication standard IEC 61580 to be serviced in a dedicated core. 
The design additionally shows multi-core communication between the ARM 
Cortex™-A15, Cortex™-M4 and DSP C66x™ cores of the AM572x while Linux runs on 
the A15s and TI-RTOS runs on the M4 and DSP cores.

Note: The packet switching and filtering logic are embedded in the extended API 
ICSS_EmacRxPKtInfo() in ipu1/icss_emacDrv.c which is based on pdk_am57xx_1_0_2/
packages/ti/drv/icss_emac/src/icss_emacDrv.c. The API and accordingly the 
switching and filter logic integration maybe changes in the later releases.
