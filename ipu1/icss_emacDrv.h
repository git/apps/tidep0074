/**
 *   @file  icss_emacDrv.h
 *   @brief
 *      Include file for ICSS_EMAC RX/TX functions and queue structures
 */

/* Copyright (C) 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*
*   Redistribution and use in source and binary forms, with or without 
*   modification, are permitted provided that the following conditions 
*   are met:
*
*     Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer.
*
*     Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the   
*     distribution.
*
*     Neither the name of Texas Instruments Incorporated nor the names of
*     its contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/



#ifndef ICSS_EMACDRV__H
#define ICSS_EMACDRV__H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <ti/csl/hw_types.h>


#include <ti/drv/pruss/pruicss.h>
#include <ti/drv/icss_emac/icss_emacDrv_Def.h>

#include <ti/drv/icss_emac/icss_emacCommon.h>
#include <ti/drv/icss_emac/icss_emacLearning.h>
#include <ti/drv/icss_emac/icss_emacFwInit.h>
#include <ti/drv/icss_emac/icss_emacStatistics.h>
#include <ti/drv/icss_emac/icss_emacStormControl.h>
#include <ti/drv/icss_emac/icss_emac_osal.h>

#include <ti/drv/icss_emac/src/phy.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#define PKT_PROC_NONE           0
#define PKT_PROC_GOOSE          1

#define PKT_PROC_PTP            3
#define PKT_PROC_SV             4
#define PKT_PROC_MPU            5

/**
 *  \defgroup   ErrCodes    STANDARD ERROR CODES
 *  @{
 */
/** Internal functions return error codes */
#define ICSS_EMAC_SWITCH_INSTANCE_CODE                        (0u)
/**< Switch Instance Code                                              */
#define ICSS_EMAC_SWITCH_ERROR_BASE                           (0x200001Fu)
/**< Switch Error Base                                              */
#define ICSS_EMAC_SWITCH_ERROR_CODE                           ((ICSS_EMAC_SWITCH_ERROR_BASE | ((ICSS_EMAC_SWITCH_INSTANCE_CODE) << 16)))
/**< Switch Error Code                                              */
#define ICSS_EMAC_SWITCH_ERROR_INFO                           (ICSS_EMAC_SWITCH_ERROR_CODE)
/**< Switch Error/Informational                                              */
#define ICSS_EMAC_SWITCH_ERROR_WARNING                        (ICSS_EMAC_SWITCH_ERROR_CODE | 0x10000000u)
/**< Switch Error/Warning                                              */
#define ICSS_EMAC_SWITCH_ERROR_MINOR                          (ICSS_EMAC_SWITCH_ERROR_CODE | 0x20000000u)
/**< Switch Error Minor                                              */
#define ICSS_EMAC_SWITCH_ERROR_MAJOR                          (ICSS_EMAC_SWITCH_ERROR_CODE | 0x30000000u)
/**< Switch Error Major                                              */
#define ICSS_EMAC_SWITCH_ERROR_CRITICAL                       (ICSS_EMAC_SWITCH_ERROR_CODE | 0x40000000u)
/**< Switch Error Critical                                              */

/**  Success code */
#define ICSS_EMAC_SWITCH_SUCCESS                              0u

/**  Error codes */
#define ICSS_EMAC_ERR_DEV_ALREADY_INSTANTIATED(instID)  (0x30000000u + ICSS_EMAC_SWITCH_ERROR_BASE + ((instId) << 16) )
/**< Device with same instance ID already created. */
#define ICSS_EMAC_ERR_DEV_NOT_INSTANTIATED              (ICSS_EMAC_SWITCH_ERROR_MAJOR + 1u)
/**< Device is not instantiated yet.                                        */
#define ICSS_EMAC_ERR_SWITCH_INVALID_PARAM                            (ICSS_EMAC_SWITCH_ERROR_MAJOR + 2u)
/**< Function or calling parameter is invalid                               */
#define ICSS_EMAC_ERR_CH_INVALID                        (ICSS_EMAC_SWITCH_ERROR_CRITICAL + 3u)
/**< Channel number invalid                                                 */
#define ICSS_EMAC_ERR_CH_ALREADY_INIT                   (ICSS_EMAC_SWITCH_ERROR_MAJOR + 4u)
/**< Channel already initialized and setup                                  */
#define ICSS_EMAC_ERR_TX_CH_ALREADY_CLOSED              (ICSS_EMAC_SWITCH_ERROR_MAJOR + 5u)
/**< Tx Channel already  closed. Channel close failed                       */
#define ICSS_EMAC_ERR_TX_CH_NOT_OPEN                   (ICSS_EMAC_SWITCH_ERROR_MAJOR + 6u)
/**< Tx Channel not open.                                                   */
#define ICSS_EMAC_ERR_TX_NO_LINK                       (ICSS_EMAC_SWITCH_ERROR_MAJOR + 7u)
/**< Tx Link not up.                                                        */
#define ICSS_EMAC_ERR_TX_OUT_OF_BD                     (ICSS_EMAC_SWITCH_ERROR_MAJOR + 8u)
/**< Tx ran out of Buffer descriptors to use.                               */
#define ICSS_EMAC_ERR_RX_CH_INVALID                    (ICSS_EMAC_SWITCH_ERROR_CRITICAL + 9u)
/**< Rx Channel invalid number.                                             */
#define ICSS_EMAC_ERR_RX_CH_ALREADY_INIT               (ICSS_EMAC_SWITCH_ERROR_MAJOR + 10u)
/**< Rx Channel already setup.                                              */
#define ICSS_EMAC_ERR_RX_CH_ALREADY_CLOSED             (ICSS_EMAC_SWITCH_ERROR_MAJOR + 11u)
/**< Rx Channel already closed. Channel close failed.                       */
#define ICSS_EMAC_ERR_RX_CH_NOT_OPEN                   (ICSS_EMAC_SWITCH_ERROR_MAJOR + 12u)
/**< Rx Channel not open yet.                                               */
#define ICSS_EMAC_ERR_DEV_ALREADY_CREATED              (ICSS_EMAC_SWITCH_ERROR_MAJOR + 13u)
/**< EMAC device already created.                                           */
#define ICSS_EMAC_ERR_DEV_NOT_OPEN                     (ICSS_EMAC_SWITCH_ERROR_MAJOR + 14u)
/**< Device is not open or not ready                                        */
#define ICSS_EMAC_ERR_DEV_ALREADY_CLOSED               (ICSS_EMAC_SWITCH_ERROR_MAJOR + 15u)
/**< Device close failed. Device already closed.                            */
#define ICSS_EMAC_ERR_DEV_ALREADY_OPEN                 (ICSS_EMAC_SWITCH_ERROR_MAJOR + 16u)
/**< Device open failed. Device already open.                               */
#define ICSS_EMAC_ERR_RX_BUFFER_ALLOC_FAIL             (ICSS_EMAC_SWITCH_ERROR_CRITICAL +17u)
/**< Rx Buffer Descriptor allocation failed.                                */
#define ICSS_EMAC_SWITCH_INTERNAL_FAILURE                     (ICSS_EMAC_SWITCH_ERROR_MAJOR + 18u)
/**< EMAC Internal failure.                                                 */
#define ICSS_EMAC_SWITCH_VLAN_UNAWARE_MODE          (ICSS_EMAC_SWITCH_ERROR_MAJOR + 19u)
/**< VLAN support not enabled in EMAC                                       */
#define ICSS_EMAC_SWITCH_ALE_TABLE_FULL             (ICSS_EMAC_SWITCH_ERROR_MAJOR + 20u)
/**< ALE Table full.                                               */
#define ICSS_EMAC_SWITCH_ADDR_NOTFOUND              (ICSS_EMAC_SWITCH_ERROR_MAJOR + 21u)
/**< Multicast/Unicast/OUI Address not found in ALE.                        */
#define ICSS_EMAC_SWITCH_INVALID_VLANID             (ICSS_EMAC_SWITCH_ERROR_MAJOR + 22u)
/**< Invalid VLAN Id.                                                       */
#define ICSS_EMAC_SWITCH_INVALID_PORT               (ICSS_EMAC_SWITCH_ERROR_MAJOR + 23u)
/**< Invalid Port Specified.                                                */
#define ICSS_EMAC_SWITCH_BD_ALLOC_FAIL              (ICSS_EMAC_SWITCH_ERROR_MAJOR + 24u)
/**< Buffer Descriptor Allocation failure. OOM                              */
#define ICSS_EMAC_ERR_BADPACKET                        (ICSS_EMAC_SWITCH_ERROR_MAJOR + 25u)
/**< Supplied packet was invalid  */
#define ICSS_EMAC_ERR_COLLISION_FAIL                   (ICSS_EMAC_SWITCH_ERROR_MAJOR + 26u)
/**< Collision queue was full                             */
#define ICSS_EMAC_ERR_MACFATAL                         (ICSS_EMAC_SWITCH_ERROR_CRITICAL + 26u)
/**< Fatal Error - EMACClose() required                                    */
/* @} */

/**Dual MAC Mode. Both Ports Enabled  */
#define ICSS_EMAC_MODE_DUALMAC   (4U)
/**witch Mode  */
#define ICSS_EMAC_MODE_SWITCH    (3U)
/**Single EMAC Mode. Port 1 Enabled  */
#define ICSS_EMAC_MODE_MAC1      (1U)
/**Single EMAC Mode. Port 2 Enabled  */
#define ICSS_EMAC_MODE_MAC2      (2U)

/** Number of Ports in a single ICSS  */
#define MAX_PORT_NUMBER     2

/**Total Queues available*/
#define ICSS_EMAC_NUMQUEUES (5)
/**Priority Queue 1*/
#define ICSS_EMAC_QUEUE1 (0)
/**Priority Queue 2*/
#define ICSS_EMAC_QUEUE2 (1)
/**Priority Queue 3*/
#define ICSS_EMAC_QUEUE3 (2)
/**Priority Queue 4*/
#define ICSS_EMAC_QUEUE4 (3)
/**Collision Queue*/
#define ICSS_EMAC_COLQUEUE (4)


/*
*  @brief     ICSS EMAC Init Configuration Structure
*/
typedef struct {
    /** Phy address of the ports.For mac each handle will have single port only
    *   And Two for Switch handle
    */
    uint32_t phyAddr[MAX_PORT_NUMBER];
    /** Flag to enable Half duplex capability. Firmware support also is required to
    * 	enable the functionality
    */
    uint8_t halfDuplexEnable;
    /** Flag to enable Interrupt pacing */
    uint8_t enableIntrPacing;
    /** Number of packets threshold for Pacing Mode1 */
    uint16_t pacingThreshold;
    /** Queue Priority separation for RT and NRT packets
    * 	If packets are in Queue <=ethPrioQueue, they will be forwarded to NRT callback
    * 	and others to RT callback
    */
    uint8_t ethPrioQueue;
    /** Flag to enable learning. Not applicable for Mac mode */
    uint8_t learningEn;
    /**Port Mask. Indication to LLD which ports to be used
    * Valid values:ICSS_EMAC_MODE_SWITCH,ICSS_EMAC_MODE_MAC1,ICSS_EMAC_MODE_MAC1
    */
    uint8_t portMask;
    /**Rx interrupt number*/
    uint8_t rxIntNum;
    /**Link interrupt number*/
    uint8_t linkIntNum;
    /**Tx completion interrupt number*/
    uint8_t txIntNum;
    /**Macid to be used for the interface*/
    uint8_t* macId;
    /**Pacing mode to be used(MODE1/MODE2)*/
    uint8_t ICSS_EmacIntrPacingMode;
} ICSS_EmacInitConfig;

#define ICSSEMAC_InitConfig ICSS_EmacInitConfig


/**
 * @brief Queue Statistics
 */
typedef struct {
    /**queue raw count*/
    uint32_t rawCount;
    /**queue error count*/
    uint32_t errCount;
} ICSS_EmacQueueStats;


/**
 * @brief Queue Parameters
 */
typedef struct queParams {
    /**Queue statistics*/
    ICSS_EmacQueueStats  qStat;
    /**buffer  offset*/
    uint16_t    buffer_offset;
    /**buffer descriptor offset*/
    uint16_t    buffer_desc_offset;
    /**queue descriptor offset*/
    uint16_t    queue_desc_offset;
    /**queue size*/
    uint16_t    queue_size;
} ICSS_EmacQueueParams;

/**
 * @brief Port parameters
 */
typedef struct {
    /**pointer to PTCP packet mem*/
    uint8_t*            ptcpPktBuff;
    /**statistics   - raw*/
    uint32_t            rawCount;
    /**Error count*/
    uint32_t            errCount;
    /**Queues per port*/
    ICSS_EmacQueueParams        queue[ICSS_EMAC_NUMQUEUES];
} ICSS_EmacPortParams;

/*
*  @brief     ICSSEMAC_Object
*             Handle containing pointers to all modules as well as Initialization Config
*/
typedef struct ICSS_EmacObject_s {
    /*! PRUICSS Handle details where the EMAC driver will be based on       */
    PRUICSS_Handle            pruIcssHandle;
    /*! Pointer to initial configuration structure                          */
    ICSS_EmacInitConfig       *emacInitcfg;
    /*! Mac Table Pointer for Learning module. Not applicable for Emac mode */
    HashTable_t               *macTablePtr;
    /*! Pointer All Driver specific Callback  structure                     */
    ICSS_EmacCallBackObject   *callBackHandle;
    /*! Pointer to  Emac driver Firmware statistics structure               */
    void                      *pruStat;
    /*! Pointer to Emac Driver host statistics structure                    */
    void                      *hostStat;
    /*! Pointer to Storm Prevention structure */
    void                      *stormPrevPtr;
    /*! Rx Task Handle for the emac instance.Required for receiving packets */
    void                      *rxTaskHandle;
    /*! Tx Task Handle for the emac instance.Required for notification of Trasnmit Complete indication from  PRUICSS firmware */
    void                      *txTaskHandle;
    /*! Rx Semaphore Handle for the emac instance.Required for receiving packets */
    void                      *rxSemaphoreHandle;
    /*! Tx Complete Semaphore Handle for the emac instance.Required for notification of Trasnmit Complete indication from  PRUICSS firmware */
    void                      *txSemaphoreHandle;
    /*! Rx interrupt handler */
    void                      *rxintHandle;
    /*! Link interrupt handler */
    void                      *linkintHandle;

    /*! Tx Complete interrupt handler */
    void                      *txintHandle;
    
    /*! Pointer to store any data from High Level Driver  */
    void                      *pvtInfo;
    /*! Link status for the ports */
    uint8_t                   linkStatus[MAX_PORT_NUMBER];

    ICSS_EmacCallBack port0ISRCall;
    ICSS_EmacCallBack port1ISRCall;

    ICSS_EmacCallBack icssEmacHwIntRx;
    ICSS_EmacCallBack icssEmacHwIntTx;
    void *port0ISRUser;
    void *port1ISRUser;
    ICSS_EmacPortParams switchPort[3];
} ICSS_EmacObject;

/* For backward compatibility */
#define ICSSEMAC_Object ICSS_EmacObject
/**
 * @brief Handle containing base addresses for all memories and modules
 */
typedef struct ICSS_EmacHwAttrs_s {
    ICSS_EmacBaseAddressHandle_T emacBaseAddrCfg;
}ICSS_EmacHwAttrs;
/* For backward compatibility */
#define ICSSEMAC_HwAttrs ICSS_EmacHwAttrs

/**
 * @brief Base EMAC handle containing pointers to all modules required for driver to work
 */
typedef struct ICSS_EmacConfig_s {
    /*! Pointer to a driver specific data object */
    void                   *object;

    /*! Pointer to a driver specific hardware attributes structure */
    void          const    *hwAttrs;

} ICSS_EmacConfig;

/**
 * @brief Rx packet processing information block that needs to passed into  call to ICSS_EmacRxPktGet
 */
typedef struct
{
    ICSS_EmacHandle icssEmacHandle; /*! handle to ICSS_EMAC Instance*/
    uint32_t destAddress; /*! Base address of data buffer where received frame has to be stored */
    uint8_t queueNumber;    /*!Receive queue from which frame has to be copied */
    uint8_t port;   /*!Returns port number on which frame was received */
    uint32_t more;  /*!Returns more which is set to 1 if there are more frames in the queue */
} ICSS_EmacRxArgument;

/**
 * @brief Tx packet processing information block that needs to passed into  call to ICSS_EmacTxPacket
 */
typedef struct
{
    ICSS_EmacHandle icssEmacHandle; /*! handle to ICSS_EMAC Instance*/
    const uint8_t *srcAddress;  /*! Base address of the buffer where the frame to be transmitted resides */
    uint8_t portNumber; /*!  Port on which frame has to be transmitted */
    uint8_t queuePriority;  /*! Queue number in which frame will be  queued for transmission */
    uint16_t lengthOfPacket;    /*! length of the frame in bytes */
} ICSS_EmacTxArgument;

/* For backward compatibility */
#define ICSSEMAC_Config ICSS_EmacConfig

/**
* @brief Enum for enableIntrPacing
*/
typedef enum {
	ICSS_EMAC_ENABLE_PACING = 0,		/**< Interrupt pacing enabled*/
	ICSS_EMAC_DISABLE_PACING			/**< Interrupt pacing disabled*/
} intrPacing;

/**
* @brief Enum for ICSS_EmacIntrPacingMode
*/
typedef enum {
	ICSS_EMAC_INTR_PACING_MODE1 = 0,    /**< Frame Count based Interrupt pacing*/
	ICSS_EMAC_INTR_PACING_MODE2         /**< Timer based Interrupt pacing*/
} ICSS_EmacIntrPacingMode_e;




/**Maximum Valid size (incl header + VLAN TAG..., no CRC)*/
#define ICSS_EMAC_MAXMTU  (1518U)
/**Minimum Valid size ( DA + SA + Ethertype)*/
#define ICSS_EMAC_MINMTU  (14)

/**
 *  @def  ICSS_EMAC_PORT_0
 *        Used to specify host side port
 */
#define ICSS_EMAC_PORT_0 (0)

/**
 *  @def  ICSS_EMAC_PORT_1
 *        Used to specify physical port 1 MII 0 (tx)
 */
#define ICSS_EMAC_PORT_1 (1)

/**
 *  @def  ICSS_EMAC_PORT_2
 *        Used to specify physical port 2 MII 1 (tx)
 */
#define ICSS_EMAC_PORT_2 (2)


/**
* @def MAX_NUM_PROTOCOL_IMPLEMENTED
*       Max Number of protocols
*/
#define MAX_NUM_PROTOCOL_IMPLEMENTED   (50U)
/**
* @def NUM_PROTOCOLS_IMPLEMENTED
*      Number of protocols supported
*/
#define NUM_PROTOCOLS_IMPLEMENTED  (2U)
/**
* @def IP4_PROT_TYPE
*      IP4 Protcol type
*/
#define IP4_PROT_TYPE	(0x800)
/**
* @def ARP_PROT_TYPE
*      ARP protocol type
*/
#define ARP_PROT_TYPE	(0x806)


/**
 *  @b Description
 *  @n
 *      Receive frame interrupt service routine
 *
 *  @param[in]  args parameter list to interrupt service routine
 *  @retval     none
 */
void ICSS_EmacRxInterruptHandler(void *args);



/**
 *  @b Description
 *  @n
 *      Transmit complete frame interrupt service routine
 *
 *  @param[in]  args parameter list to interrupt service routine
 *  @retval     none
 */
void ICSS_EmacTxInterruptHandler(void *args);



/**
* @brief Link change status interrupt for Port 0 and Port 1
*  	   calls a user callback if defined to provide link info to stack
*
* @param arg
*
* @retval none
*/
void ICSS_EmacLinkISR(void* arg);

/**
 *  @b Description
 *  @n
 *      Retrieves a frame from a host queue and copies it
 *           in the allocated stack buffer
 *
 *  @param[in]  rxArg defined at @ref ICSS_EmacRxArgument
 *  @param[in]  userArg custom Rx packet callback packet options only required for custom RxPacket implementations,
                default to NULL when calling ICSS_EmacRxPktGet which is default Tx Packet API
 *  @retval     Length of the frame received in number of bytes or -1 on Failure
 */
int32_t ICSS_EmacRxPktGet(ICSS_EmacRxArgument *rxArg, void* userArg);


/**
 *  @b Description
 *  @n
 *      Finds the maximum fill level of the queue in terms of 32 byte blocks.
        For example, if there was only one 64 byte packet received when this
        API is called then it would return value of 2.
        It also returns number of times queue has overflown.
 *
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance.
 *  @param[in]  portNumber    Port on which queue resides. Valid values are:
                              0 == PORT0, 1 == PORT1, 2 == PORT2
 *  @param[in]  queuePriority   Priority of the queue or queue number whose fill level has to be found
 *  @param[in]  queueType   Rx/Tx Queue
 *  @param[out]  queueOverflowCount    Number of times queue has overflown

 *  @retval     The maximum fill level of the queue in terms of 32 byte blocks or
 *              <0 if there was an error in the input parameters
 */
int32_t ICSS_EmacGetMaxQueueFillLevel(ICSS_EmacHandle icssEmacHandle,int32_t portNumber, int32_t queuePriority,uint8_t queueType, int32_t* queueOverflowCount);

/**
 *  @b Description
 *  @n
 *      API to register the hardware interrupt receive packet callback function
 *
 *  @param[in]  hwIntRx    hardware interrupt receive packet callback function
 *  @retval     none
 */
void ICSS_EmacRegisterHwIntRx (ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack hwIntRx);

/**
 *  @b Description
 *  @n
 *       API to retrieve the information about the received frame which
 *       is then used to dequeue the frame from the host queues
 *
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance.
 *  @param[out]  portNumber    Return pointer of port number where frame was received
 *  @param[out]  queueNumber   Return pointer of host queue where the received frame is queued
 *  @param[ou]   pktProc       Return pointer of packet type
 *  @retval     none
 */
int32_t ICSS_EmacRxPktInfo(ICSS_EmacHandle icssEmacHandle,
                           int32_t* portNumber,
                           int32_t* queueNumber,
                           int32_t* pktProc);
/**
 *  @b Description
 *  @n
 *      API to queue a frame which has to be transmitted on the
 *      specified port queue
 *
 *  @param[in]  txArg defined at @ref ICSS_EmacTxArgument
 *  @param[in]  userArg custom Tx packet callback packet options only required for custom TxPacket implementations,
                default to NULL when calling ICSS_EmacTxPacket which is default Tx Packet API
 *  @retval     0 on scuess,  <0 on failure
 */
int32_t ICSS_EmacTxPacket(ICSS_EmacTxArgument *txArg, void* userArg);




/* Local Functions */
/** @brief  API to copy a packet from DDR to Tx Queue memory on L3 and synchronize with
 *  firmware
 *  @internal
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance.
 *  @param[in]  srcAddress    Base address of the buffer where the frame to be transmitted resides
 *  @param[in]  portNumber   Port on which frame has to be transmitted.
 *                            Valid values are:
                              1 == PORT1, 2 == PORT2
 *  @param[in]  queuePriority    Queue number in which frame will be
 *                               queued for transmission
 *  @param[in] lengthOfPacket   length of the frame in bytes
 *  @retval     0 on scuess,  <0 on failure
 */
int32_t ICSS_EmacTxPacketEnqueue (ICSS_EmacHandle icssEmacHandle,
                                  const uint8_t* srcAddress,
                                  uint8_t portNumber,
                                  uint8_t queuePriority,
                                  uint16_t lengthOfPacket);
/**
 *  @brief  Task to enable copying of Rx data from queues to DDR. This is a function that calls
 *  another function to empty the queues
 *  @internal
 *  @param[in]  a0 Generic argument
 *  @param[in]  a1    Generic argument
 *  @retval     none
 */
void ICSS_EMacOsRxTaskFnc(uint32_t a0, uint32_t a1);


/**
 *  @brief  Task to enable receiving Indication of transmit packet complete by PRU-ICSS firmware. 
 *  @internal
 *  @param[in]  a0 Generic argument
 *  @param[in]  a1    Generic argument
 *  @retval     none
 */
void ICSS_EMacOsTxTaskFnc(uint32_t a0, uint32_t a1);


/**
 *  @brief Function to initialize all Port Queue params
 *  @internal
 *  @retval 0 on success
 */
int32_t ICSS_EmacPortInit(ICSS_EmacHandle icssEmacHandle);

/**
 *  @brief Function to initialize Host Port Queue params
 *  @internal
 *  @param icssEmacHandle pointer to ICSS EMAC handle
 *  @retval 0 on success
 */
void ICSS_EmacHostInit(ICSS_EmacHandle icssEmacHandle);

/**
 *  @brief Function to initialize MAC Port Queue params
 *  @internal
 *  @param portNum Port number
 *  @retval 0 on success
 */
void ICSS_EmacMACInit(ICSS_EmacHandle icssEmacHandle, uint8_t portNum);
/**
 *  @brief Function to add Ether Type to approved list of protocols
 *  @internal
 *  @param icssEmacHandle pointer to ICSS EMAC handle
 *  @param protocolType 16 bit value indicating protocol type. IEEE format
 *  @retval 0 on success
 */
void addProtocolToList(uint16_t protocolType);
/**
 *  @brief Function to delete Rx semaphore and Task
 *  @internal
 *  @param icssEmacHandle pointer to ICSS EMAC handle
 *  @retval 0 on success
 */
int8_t ICSS_EmacOSDeInit(ICSS_EmacHandle icssEmacHandle);
/**

 *  @brief Function to initialize Rx semaphore and Task
 *  @internal
 *  @param icssEmacHandle pointer to ICSS EMAC handle
 *  @retval 0 on success
 */
int8_t ICSS_EmacOSInit(ICSS_EmacHandle icssEmacHandle);

/**
* @brief Callback function to process protocol specific handler for link status ISR
* @param icssEmacHandle pointer to ICSS EMAC handle
* @param callBack	Callback function pointer
* @param userArg	user specific parameter
*
* @retval none
*/
void ICSS_EmacRegisterPort0ISRCallback(ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack callBack, void *userArg);
/**
* @brief Callback function to process protocol specific handler for link status ISR
*
* @param icssEmacHandle pointer to ICSS EMAC handle
* @param callBack	Callback function pointer
* @param userArg	user specific parameter
*
* @retval none
*/
void ICSS_EmacRegisterPort1ISRCallback(ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack callBack, void *userArg);


/**
* @brief Function to status of Phy Link
*
* @param mdioBaseAddr       mdio subsystem base address
* @param phyAddr            physical address
* @param retries            retry count
*
* @retval 1 if phy link up, 0 phy link down
*/
uint32_t ICSS_EmacPhyLinkStatusGet(uint32_t mdioBaseAddr,
                                     uint32_t phyAddr,
                                     volatile uint32_t retries);


/**
 *  @b Description
 *  @n
 *      API to register the hardware interrupt for Transmit packet complete by PRU-ICSS firmware
 *
 *  @param[in]  hwIntRx    hardware interrupt transmit packet complete callback function
 *  @retval     none
 */
void ICSS_EmacRegisterHwIntTx( ICSS_EmacHandle icssEmacHandle, ICSS_EmacCallBack hwIntTx);


/**
 *  @b Description
 *  @n
 *      API Function to re-initialize all Port Queue params
 * 
 *  @param[in]  icssEmacHandle handle to ICSS_EMAC Instance
 *  @param[in]  portNumber number of the port to flush
 *  @retval None
 */
void ICSS_EmacPortFlush(ICSS_EmacHandle icssEmacHandle, int32_t portNumber);
#ifdef __cplusplus
}
#endif

#endif /* _ICSS_EMACDRV_H_ */
