/*
 * Copyright (C) 2016 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/knl/Cache.h>

#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/hal/Core.h>

#include <ti/csl/src/ip/icss/V0/cslr_icss_intc.h>

#include <ti/drv/pruss/soc/pruicss_v1.h>

#include <icss_emacDrv.h>

#ifdef SOC_AM572x
#include <ti/drv/icss_emac/firmware/am57x/v1_0/icss_emac_pru0_bin.h>
#include <ti/drv/icss_emac/firmware/am57x/v1_0/icss_emac_pru1_bin.h>
#include <ti/drv/pruss/soc/am572x/pruicss_device.c>
#include <ti/drv/icss_emac/soc/am572x/icss_emacSoc.c>
#include <ti/csl/soc/am572x/src/csl_device_xbar.h>
#define PRU0_FIRMWARE_V1_0_NAME      PRU0_FIRMWARE_V1_0 
#define PRU1_FIRMWARE_V1_1_NAME      PRU1_FIRMWARE_V1_0 

#include <ti/drv/icss_emac/firmware/am57x/v2_1/icss_emac_pru0_bin.h>
#include <ti/drv/icss_emac/firmware/am57x/v2_1/icss_emac_pru1_bin.h>
#define PRU0_FIRMWARE_NAME      PRU0_FIRMWARE
#define PRU1_FIRMWARE_NAME      PRU1_FIRMWARE
#endif

#ifdef SOC_AM571x
#include <ti/drv/icss_emac/firmware/am57x/v2_1/icss_emac_pru0_bin.h>
#include <ti/drv/icss_emac/firmware/am57x/v2_1/icss_emac_pru1_bin.h>
#include <ti/drv/pruss/soc/am571x/pruicss_device.c>
#include <ti/drv/icss_emac/soc/am571x/icss_emacSoc.c>
#include <ti/csl/soc/am571x/src/csl_device_xbar.h>
#define PRU0_FIRMWARE_NAME      PRU0_FIRMWARE
#define PRU1_FIRMWARE_NAME      PRU1_FIRMWARE
#endif
#include <ti/drv/icss_emac/test/src/tiemac_pruss_intc_mapping.h>

#include <ti/drv/icss_emac/test/src/icss_switch_emac.h>
#include <ti/drv/icss_emac/test/src/test_mdio.c>
#include <ti/drv/icss_emac/test/src/icss_switch_emac.c>
#include <ti/drv/icss_emac/test/src/icss_emac_osal.c>

#include "Server.h"

PRUICSS_Handle pruIcssHandle;
ICSS_EmacHandle emachandle;
ICSS_EmacHandle emachandle1;

void SOCCtrlGetPortMacAddr(uint32_t portNum, uint8_t *pMacAddr);
void ICSSEmacDRVInit(ICSS_EmacHandle handle, uint8_t instance);
extern ICSS_EmacBaseAddrCfgParams icss_EmacBaseAddrCfgParams[3];
void InterruptInit(ICSS_EmacHandle icssEmacHandle);
void InterruptEnd(ICSS_EmacHandle icssEmacHandle);
static void EnableEMACInterrupts(ICSS_EmacHandle icssemacHandle);
void DisableEMACInterrupts(ICSS_EmacHandle icssemacHandle);
inline void clearLink0ISR(ICSS_EmacHandle icssemacHandle);
inline void clearLink1ISR(ICSS_EmacHandle icssemacHandle);

#define MAX_TEST_PKT_NUM        12
#define TEST_PKT_SIZE           256

#define GOOSE_ENABLE		0xBEEFBEEF
#define GOOSE_DISABLE		0xDEADDEAD

uint32_t packetLength = 0;
uint32_t testNum = 0;

void *icssRxSem;
void *icssRxSem1;
extern uint32_t *pRxPkt;
extern uint32_t *pTxPkt;

extern uint32_t *pGoosePara;
extern uint32_t bGooseFilter;

uint8_t lclMac[6];
uint8_t lclMac1[6];

uint32_t GooseBuf[MAX_PKT_FRAME_SIZE];
uint32_t PtpBuf[MAX_PKT_FRAME_SIZE];
uint32_t GarbageBuf[MAX_PKT_FRAME_SIZE];

char GooseDestMacAddr[DEST_MAC_NUM][ETH_ALEN] = {
	{0x11, 0x22, 0x33, 0x44, 0x55, 0x66},
	{0x22, 0x33, 0x44, 0x55, 0x66, 0x77},
	{0x33, 0x44, 0x55, 0x66, 0x77, 0x88},
	{0x44, 0x55, 0x66, 0x77, 0x88, 0x99},
};
uint16_t GooseAPPID[ACCEPT_APPID_NUM] = {0x1111, 0x2222, 0x3333, 0x4444};

char ptpMulDest1MacAddr[ETH_ALEN] = {0x01, 0x1b, 0x19, 0x00, 0x00, 0x00};
char ptpMulDest2MacAddr[ETH_ALEN] = {0x01, 0x80, 0x02, 0x00, 0x00, 0x0F};

static const uint8_t test_pkt[MAX_TEST_PKT_NUM][TEST_PKT_SIZE] = {
	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x08, 0x06, 0x00, 0x01,	/* ARP, accept, route to A15 */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},
	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac, Goose*/
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x88, 0xb8, 0x00, 0x01,	/* Goose 0x88b8, to A15,not match mac */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},

	{
		0x11, 0x22, 0x33, 0x44, 0x55, 0x66, /* Goose mac*/
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x88, 0xb8, 0x00, 0x01,	/* Goose 0x88b8, drop,not match APPID */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},

	{
		0x01, 0x1b, 0x19, 0x00, 0x00, 0x00, /* multi-cast 01-1b mac */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x88, 0xf7, 0x00, 0x01,	/* 1588, accept */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},

	{
		0x01, 0x80, 0x02, 0x00, 0x00, 0x0f, /* multi-cast 01-80 mac */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x88, 0xf7, 0x00, 0x01,	/* 1588, accept */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},
	{
		0x11, 0x80, 0x02, 0x00, 0x00, 0x0f, /* multi-cast 11-80 mac */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x88, 0xf7, 0x00, 0x01,	/* 1588, drop */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},
	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac, SV*/
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x88, 0xba, 0x00, 0x01,	/* IEC61850, SV, drop*/
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},
	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x08, 0x06, 0x00, 0x01,	/* ARP, accept, route to A15 */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	},
	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac, to A15 */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x81, 0x00, /* type - VLAN */
		0x00, 0x00, /* VLAN tag - TCI */
		0x88, 0xb8, /* VLAN tag - Goose */
		0x11, 0x11,	/* APPID, type 1 */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},

	{
		0x11, 0x22, 0x33, 0x44, 0x55, 0x66, /* goose mac, accept */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x81, 0x00, /* type - VLAN */
		0x00, 0x00, /* VLAN tag - TCI */
		0x88, 0xb8, /* VLAN tag - Goose */
		0x11, 0x11,	/* APPID, type 1 */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},

	{
		0x11, 0x22, 0x33, 0x44, 0x55, 0x66, /* goose dest mac, accept */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x81, 0x00, /* type - VLAN */
		0x00, 0x00, /* VLAN tag - TCI */
		0x88, 0xb8, /* VLAN tag - Goose */
		0x44, 0x44,	/* APPID, type 1A */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},

	{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* broadcast mac, to A15 */
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x81, 0x00, /* type - VLAN */
		0x00, 0x00, /* VLAN tag - TCI */
		0x88, 0xb8, /* VLAN tag - Goose */
		0x11, 0x22,	/* APPID, reject */
		0x08, 0x00, 0x06, 0x04, 0x00,0x01,
		0x01, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0xc0, 0xa8, 0x01, 0x16,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xc0, 0xa8,0x01, 0x02
	},
};

/**
 * @brief configures GOOSE filter parameters is enabled
 *
 * @param none
 *
 * @retval none
 */
void Goose_para_config()
{
	uint32_t *pTempAddr;
	uint32_t len;
	int i;

	if(*pGoosePara == GOOSE_ENABLE) {
		Log_print0(Diags_INFO, "Enable/configure Goose filtering...");
		bGooseFilter = 1;
		pTempAddr = pGoosePara + 1;
		len = DEST_MAC_NUM * ETH_ALEN;
		memcpy(GooseDestMacAddr, pTempAddr, len);

		for(i=0; i<6; i++)
			Log_print1(Diags_INFO, "Dest Mac Addr[0] = 0x%x\n", 
					GooseDestMacAddr[0][i]);

		pTempAddr = pGoosePara + 1 + len/4;
		len = ACCEPT_APPID_NUM * 2;
		memcpy(GooseAPPID, pTempAddr, len);

		for(i=0; i<4; i++)
			Log_print1(Diags_INFO, "GooseAPPID = 0x%x", 
					GooseAPPID[i]);
	}
	else if(*pGoosePara == GOOSE_DISABLE) {
		Log_print0(Diags_INFO, "Disable Goose filtering");
		bGooseFilter = 0;
	}
	else
		Log_print0(Diags_INFO, "Use default Goose filtering config");

	/* clear Goose Flag */
	*pGoosePara = 0;
}

/**
 * @brief transmit packets from IPU
 *
 * @param none
 *
 * @retval none
 */
void icss_tx()
{
	ICSS_EmacTxArgument txArgs;

	/* Send out test packet for loopback */
	txArgs.icssEmacHandle = emachandle;
	txArgs.lengthOfPacket = TEST_PKT_SIZE;
	txArgs.portNumber = 1;
	txArgs.queuePriority = 3;
	txArgs.srcAddress = &test_pkt[testNum][0];

	ICSS_EmacTxPacket(&txArgs, NULL);
	testNum++;
	if ( testNum >= MAX_TEST_PKT_NUM)
		testNum = 0;
}

/**
 * @brief transmit packets received from host MPU
 *
 * @param pTxPacket: trasmit packet buffer pointer
 *        TxPacketLen: trasmit packet size
 *
 * @retval none
 */
void icss_tx_mpu(UInt32 *pTxPacket, Int TxPacketLen)
{
	ICSS_EmacTxArgument txArgs;

	Log_print0(Diags_INFO, "Sending packet from A15...");

	txArgs.icssEmacHandle = emachandle;
	txArgs.lengthOfPacket = TxPacketLen;
	txArgs.portNumber = 1;
	txArgs.queuePriority = 3;
	txArgs.srcAddress = (const uint8_t *)pTxPacket;

	ICSS_EmacTxPacket(&txArgs, NULL);
}

/**
 * @brief post semaphore when a packet is received
 *
 * @param none
 *
 * @retval none
 */
void icss_rx()
{
	SemaphoreP_pend(icssRxSem, SemaphoreP_WAIT_FOREVER);
	Log_print0(Diags_INFO, "Packet Received!");
}


/**
 * @brief packet reception callback function
 *
 * @param queueNum: receive queue number
 *        ICSS_EmacSubSysHandle: ICSS_EMAC handler
 *
 * @retval none
 */
void testCallbackRxPacket(uint32_t *queueNum, uint32_t ICSS_EmacSubSysHandle)
{
	int32_t         port;
	uint32_t        queue;
	int32_t         pkt_proc;
	int32_t	        pktLen;
	uint32_t 	*pProcRxPkt;
	ICSS_EmacRxArgument rxArgs;

	queue = (*queueNum);
	packetLength = 0;

	pktLen = ICSS_EmacRxPktInfo((ICSS_EmacHandle)ICSS_EmacSubSysHandle, 
					&port, (int32_t *)&queue, &pkt_proc);

	if(pktLen > 0) {
		if(pkt_proc == PKT_PROC_MPU) {
			Log_print0(Diags_INFO, "place packet in A15 queue");
			pProcRxPkt = pRxPkt;
			packetLength = pktLen;
		}
		else if(pkt_proc == PKT_PROC_GOOSE) {
			Log_print0(Diags_INFO, "place packet in Goose buffer");
			pProcRxPkt = &GooseBuf[0];
		}
		else if(pkt_proc == PKT_PROC_PTP) {
			Log_print0(Diags_INFO, "place packet in PTP buffer");
			pProcRxPkt = &PtpBuf[0];
		}
		else if(pkt_proc == PKT_PROC_SV) {
			Log_print0(Diags_INFO, "place packet in SV buffer");
			pProcRxPkt = &GarbageBuf[0];
		}
		else {
			Log_print0(Diags_INFO, "place packet in garbage buffer");
			pProcRxPkt = &GarbageBuf[0];
		}

		rxArgs.icssEmacHandle = (ICSS_EmacHandle)ICSS_EmacSubSysHandle;
		rxArgs.destAddress =  (uint32_t)pProcRxPkt;
		rxArgs.queueNumber = *((uint32_t *)(queueNum));
		rxArgs.more = 0;
		rxArgs.port = 0;
		ICSS_EmacRxPktGet(&rxArgs, NULL);
	}

	if((ICSS_EmacHandle)ICSS_EmacSubSysHandle == emachandle) {
		SemaphoreP_post(icssRxSem);
	}
	else if((ICSS_EmacHandle)ICSS_EmacSubSysHandle == emachandle1)
	{
		SemaphoreP_post(icssRxSem1);
	}
}

/*
 *    ---task to initialize PRU---
 */
Void taskPruss(UArg a0, UArg a1)
{
	Uint8 firmwareLoad_done = FALSE;

	Log_print0(Diags_INFO, "start task PRU-ICSS\n");

	uint32_t pgVersion = (VHW_RD_REG32(
				CSL_MPU_CTRL_MODULE_WKUP_CORE_REGISTERS_REGS + 
				CTRL_WKUP_ID_CODE) & 0xf0000000) >> 28;
	PRUICSS_pruDisable(pruIcssHandle, ICSS_EMAC_PORT_1-1);
	PRUICSS_pruDisable(pruIcssHandle, ICSS_EMAC_PORT_2-1);

#ifdef SOC_AM572x
	if (pgVersion >= 2)
	{
		Log_print0(Diags_INFO, "taskPruss: Load FW to PG2.0 AM572x\n");
		if(PRUICSS_pruWriteMemory(pruIcssHandle,PRU_ICSS_IRAM(0), 0,
					(uint32_t *) PRU0_FIRMWARE_NAME,
					sizeof(PRU0_FIRMWARE_NAME)))
		{
			if(PRUICSS_pruWriteMemory(pruIcssHandle,
						PRU_ICSS_IRAM(1), 0,
						(uint32_t *) PRU1_FIRMWARE_NAME,
						sizeof(PRU1_FIRMWARE_NAME)))
				firmwareLoad_done = TRUE;
		}
	}
	else
	{
		if(PRUICSS_pruWriteMemory(pruIcssHandle,PRU_ICSS_IRAM(0), 0,
					(uint32_t *) PRU0_FIRMWARE_V1_0_NAME,
					sizeof(PRU0_FIRMWARE_V1_0_NAME)))
		{
			if(PRUICSS_pruWriteMemory(pruIcssHandle,
					PRU_ICSS_IRAM(1), 0,
					(uint32_t *) PRU1_FIRMWARE_V1_1_NAME,
					sizeof(PRU1_FIRMWARE_V1_1_NAME)))
				firmwareLoad_done = TRUE;
		}
	}
#else
	if(PRUICSS_pruWriteMemory(pruIcssHandle,PRU_ICSS_IRAM(0) ,0,
				(uint32_t *) PRU0_FIRMWARE_NAME,
				sizeof(PRU0_FIRMWARE_NAME)))
	{
		if(PRUICSS_pruWriteMemory(pruIcssHandle,PRU_ICSS_IRAM(1) ,0,
					(uint32_t *) PRU1_FIRMWARE_NAME,
					sizeof(PRU1_FIRMWARE_NAME)))
			firmwareLoad_done = TRUE;
	}
#endif

	if( firmwareLoad_done)
	{
		PRUICSS_pruEnable(pruIcssHandle, ICSS_EMAC_PORT_1-1);
		PRUICSS_pruEnable(pruIcssHandle, ICSS_EMAC_PORT_2-1);
	}

	while (!(((ICSS_EmacObject*)emachandle->object)->linkStatus[0] | 
			((ICSS_EmacObject*)emachandle1->object)->linkStatus[0]))
	{
		Log_print0(Diags_INFO, "taskPruss: link down\n");
		Task_sleep(200);
	}

	Log_print0(Diags_INFO, "link is finally up \n");

	ICSS_EmacRegisterHwIntRx(emachandle, (ICSS_EmacCallBack)testCallbackRxPacket);
	//ICSS_EmacRegisterHwIntRx(emachandle1, testCallbackRxPacket);
}

/**
 * @brief initialize PRU & ICSS_EMAC config parameters due to MMU in IPU
 *
 * @param none
 *
 * @retval none
 */
void initPruIcssEmacCfg()
{
	uint32_t i;

	/* phys. to virt translation for prussInitCfg[0], [1]*/
	for(i=0; i<sizeof(PRUICSS_HwAttrs)/2; i++)
		*((uint32_t *)prussInitCfg + i) += VIRT0;

	prussInitCfg[0].version = 0;
	prussInitCfg[1].version = 0;

	/* phys. to virt translation for ICSS_EmacBaseAddrCfgParams[0], [1]*/
	for(i=0; i<sizeof(ICSS_EmacBaseAddrCfgParams)/2; i++)
		*((uint32_t *)icss_EmacBaseAddrCfgParams + i) += VIRT0;
}

/**
 * @brief PRU & ICSS_EMAC initialization
 *
 * @param none
 *
 * @retval 0: success
 *         -1: rx semaphore creation failed
 * 	   -2: rx task creation failed
 */
int pru_icss()
{

	Error_Block eb;
	Task_Params taskParams;
	SemaphoreP_Params semParams;

	Error_init(&eb);

#ifdef EVM_INIT
	Log_print0(Diags_INFO, "AM57x_setup");
	AM57x_setup();
#endif
	/* Pin Mux completed in U-boot */
	Log_print0(Diags_INFO, "Pin Mux Config parameters address mapping");

	initPruIcssEmacCfg();

	pruIcssHandle = PRUICSS_create(pruss_config,2);

	Task_Params_init(&taskParams);
	taskParams.priority = 15;
	taskParams.instance->name = "SwitchTask";
	Task_create(taskPruss, &taskParams, &eb);

	/*ETH0 initializations*/
	emachandle = (ICSS_EmacHandle)malloc(sizeof(ICSS_EmacConfig));

	ICSS_EmacInitConfig* switchEmacCfg;
	switchEmacCfg = (ICSS_EmacInitConfig*)
		malloc(sizeof(ICSS_EmacInitConfig));
	switchEmacCfg->phyAddr[0]=0;
	switchEmacCfg->portMask = ICSS_EMAC_MODE_MAC1;
	switchEmacCfg->ethPrioQueue = ICSS_EMAC_QUEUE1;

	/* Crosssbar confiiguration */
	*(unsigned int*)(0x4A0027E8U + VIRT0) =(unsigned int)(0x00C400CA);
	*(unsigned int*)(0x4A0027ECU + VIRT0) =(unsigned int)(0x00CB00C5);

	switchEmacCfg->halfDuplexEnable = 1;
	switchEmacCfg->enableIntrPacing = ICSS_EMAC_ENABLE_PACING;
	switchEmacCfg->ICSS_EmacIntrPacingMode = ICSS_EMAC_INTR_PACING_MODE1;
	switchEmacCfg->pacingThreshold = 100;
	switchEmacCfg->learningEn = 0;
	SOCCtrlGetPortMacAddr(0,lclMac);
	switchEmacCfg->macId = lclMac;

	ICSSEmacDRVInit(emachandle, 2); 

	switchEmacCfg->rxIntNum = 28;
	switchEmacCfg->linkIntNum=27;

	((ICSS_EmacObject*)emachandle->object)->pruIcssHandle = pruIcssHandle;
	((ICSS_EmacObject*)emachandle->object)->emacInitcfg = switchEmacCfg;

	/*ETH1 initializations*/
	emachandle1 = (ICSS_EmacHandle)malloc(sizeof(ICSS_EmacConfig));

	ICSS_EmacInitConfig* switchEmacCfg1;
	switchEmacCfg1 = (ICSS_EmacInitConfig*)
		malloc(sizeof(ICSS_EmacInitConfig));
	switchEmacCfg1->phyAddr[0]= 1;

	switchEmacCfg1->portMask = ICSS_EMAC_MODE_MAC2;
	switchEmacCfg1->ethPrioQueue = ICSS_EMAC_QUEUE3;
	switchEmacCfg1->enableIntrPacing = ICSS_EMAC_DISABLE_PACING;
	switchEmacCfg1->pacingThreshold = 100;

	switchEmacCfg1->learningEn = 0;

	SOCCtrlGetPortMacAddr(1,lclMac1);
	switchEmacCfg1->macId = lclMac1;

	ICSSEmacDRVInit(emachandle1, 2);
	switchEmacCfg1->rxIntNum = 30;;
	switchEmacCfg1->linkIntNum=29;

	((ICSS_EmacObject*)emachandle1->object)->pruIcssHandle = pruIcssHandle;
	((ICSS_EmacObject*)emachandle1->object)->emacInitcfg = switchEmacCfg1;

	PRUICSS_IntcInitData pruss_intc_initdata = PRUSS_INTC_INITDATA;
	ICSS_EmacInit(emachandle,&pruss_intc_initdata,ICSS_EMAC_MODE_MAC1);

	ICSS_EmacInit(emachandle1,&pruss_intc_initdata,ICSS_EMAC_MODE_MAC2);

	Task_Params_init(&taskParams);
	taskParams.priority = 10;
	taskParams.instance->name = (char*)"port0_rxTaskFnc";
	taskParams.stackSize = 0x1000;
	taskParams.arg0 = (UArg)emachandle;
	((ICSS_EmacObject*)emachandle->object)->rxTaskHandle = 
		Task_create(ICSS_EMacOsRxTaskFnc, &taskParams, NULL);

	if(((ICSS_EmacObject*)emachandle->object)->rxTaskHandle==NULL)
		return -2;

	Task_Params_init(&taskParams);
	taskParams.priority = 10;
	taskParams.instance->name = (char*)"port1_rxTaskFnc";
	taskParams.stackSize = 0x1000;
	taskParams.arg0 = (UArg)emachandle1;
	((ICSS_EmacObject*)emachandle1->object)->rxTaskHandle = 
		Task_create(ICSS_EMacOsRxTaskFnc, &taskParams, NULL);

	if(((ICSS_EmacObject*)emachandle1->object)->rxTaskHandle==NULL)
		return -2;

	PRUICSS_pinMuxConfig(pruIcssHandle, 0x0); 
	InterruptInit(emachandle);
	InterruptInit(emachandle1);

	EMACOpen(emachandle, 2);

	EnableEMACInterrupts(emachandle);
	EnableEMACInterrupts(emachandle1);

	semParams.mode = SemaphoreP_Mode_COUNTING;
	semParams.name= "icss_rxSemaphore";
	icssRxSem =  SemaphoreP_create(0,&semParams);;
	if(icssRxSem == NULL)
		return -1;

	semParams.mode = SemaphoreP_Mode_COUNTING;
	semParams.name= "icss_rxSemaphore1";
	icssRxSem1 =  SemaphoreP_create(0,&semParams);;
	if(icssRxSem1 == NULL)
		return -1;

	Log_print0(Diags_INFO, "main_pruss: initialization done!");
	return(0);
}

/**
 * @brief Get MAC address
 *
 * @param portNum: port number
 *        pMacAddr: MAC address data pointer
 *
 * @retval none
 */
void SOCCtrlGetPortMacAddr(uint32_t portNum, uint8_t *pMacAddr)
{
	if(portNum == 0) {
		pMacAddr[5U] =  (VHW_RD_REG32(0x4A002514)
				>> 0U) & 0xFFU;
		pMacAddr[4U] =  (VHW_RD_REG32(0x4A002514)
				>> 8U) & 0xFF;
		pMacAddr[3U] =  (VHW_RD_REG32(0x4A002514)
				>> 16U) & 0xFFU;
		pMacAddr[2U] =  (VHW_RD_REG32(0x4A002518)
				>> 0U) & 0xFFU;
		pMacAddr[1U] =  (VHW_RD_REG32(0x4A002518)
				>> 8U) & 0xFFU;
		pMacAddr[0U] =  (VHW_RD_REG32(0x4A002518)
				>> 16U) & 0xFFU;
	}
	else {
		pMacAddr[5U] =  (VHW_RD_REG32(0x4A00251c)
				>> 0U) & 0xFFU;
		pMacAddr[4U] =  (VHW_RD_REG32(0x4A00251c)
				>> 8U) & 0xFF;
		pMacAddr[3U] =  (VHW_RD_REG32(0x4A00251c)
				>> 16U) & 0xFFU;
		pMacAddr[2U] =  (VHW_RD_REG32(0x4A002520)
				>> 0U) & 0xFFU;
		pMacAddr[1U] =  (VHW_RD_REG32(0x4A002520)
				>> 8U) & 0xFFU;
		pMacAddr[0U] =  (VHW_RD_REG32(0x4A002520)
				>> 16U) & 0xFFU;
	}
}


/**
 * @brief ICSS_EMAC driver initialization
 *
 * @param handle: ICSS_EMAC handler
 *        instance: PRU instance number
 *
 * @retval none
 */
void ICSSEmacDRVInit(ICSS_EmacHandle handle, uint8_t instance) 
{
	/* LLD attributes mallocs */
	handle->object = (ICSS_EmacObject*)malloc(sizeof(ICSS_EmacObject));
	handle->hwAttrs= (ICSS_EmacHwAttrs*)malloc(sizeof(ICSS_EmacHwAttrs));

	/* Callback mallocs */
	ICSS_EmacCallBackObject* callBackObj = (ICSS_EmacCallBackObject*)
		malloc(sizeof(ICSS_EmacCallBackObject));

	callBackObj->learningExCallBack=(ICSS_EmacCallBackConfig*)
		malloc(sizeof(ICSS_EmacCallBackConfig));
	callBackObj->rxRTCallBack=(ICSS_EmacCallBackConfig*)
		malloc(sizeof(ICSS_EmacCallBackConfig));
	callBackObj->rxCallBack=(ICSS_EmacCallBackConfig*)
		malloc(sizeof(ICSS_EmacCallBackConfig));
	callBackObj->txCallBack=(ICSS_EmacCallBackConfig*)
		malloc(sizeof(ICSS_EmacCallBackConfig));
	((ICSS_EmacObject*)handle->object)->callBackHandle = callBackObj;

	/*Allocate memory for learning*/
	((ICSS_EmacObject*)handle->object)->macTablePtr = (HashTable_t*)
		malloc(NUM_PORTS * sizeof(HashTable_t));

	/*Allocate memory for PRU Statistics*/
	((ICSS_EmacObject*)handle->object)->pruStat = 
		(ICSS_EmacPruStatistics_t*)malloc(NUM_PORTS * 
				sizeof(ICSS_EmacPruStatistics_t));

	/*Allocate memory for Host Statistics*/
	((ICSS_EmacObject*)handle->object)->hostStat = 
		(ICSS_EmacHostStatistics_t*)malloc(NUM_PORTS * 
				sizeof(ICSS_EmacHostStatistics_t));

	/*Allocate memory for Storm Prevention*/
	((ICSS_EmacObject*)handle->object)->stormPrevPtr = 
		(stormPrevention_t*)malloc(NUM_PORTS * 
				sizeof(stormPrevention_t));

	/* Base address initialization */
	if(NULL == ((ICSS_EmacHwAttrs*)handle->hwAttrs)->emacBaseAddrCfg) {
		((ICSS_EmacHwAttrs*)handle->hwAttrs)->emacBaseAddrCfg =
			(ICSS_EmacBaseAddressHandle_T)
			malloc(sizeof(ICSS_EmacBaseAddrCfgParams));
	}
	ICSS_EmacBaseAddressHandle_T emacBaseAddr = 
		((ICSS_EmacHwAttrs*)handle->hwAttrs)->emacBaseAddrCfg;

	if(instance == 2)
	{
		emacBaseAddr->dataRam0BaseAddr = 
			icss_EmacBaseAddrCfgParams[instance-1].dataRam0BaseAddr;
		emacBaseAddr->dataRam1BaseAddr = 
			icss_EmacBaseAddrCfgParams[instance-1].dataRam1BaseAddr;
		emacBaseAddr->l3OcmcBaseAddr =  
			icss_EmacBaseAddrCfgParams[instance-1].l3OcmcBaseAddr;
		emacBaseAddr->prussCfgRegs =  
			icss_EmacBaseAddrCfgParams[instance-1].prussCfgRegs;
		emacBaseAddr->prussIepRegs =  
			icss_EmacBaseAddrCfgParams[instance-1].prussIepRegs;
		emacBaseAddr->prussIntcRegs = 
			icss_EmacBaseAddrCfgParams[instance-1].prussIntcRegs;
		emacBaseAddr->prussMiiMdioRegs = 
			icss_EmacBaseAddrCfgParams[instance-1].prussMiiMdioRegs;
		emacBaseAddr->prussMiiRtCfgRegsBaseAddr = 
			icss_EmacBaseAddrCfgParams[instance-1].prussMiiRtCfgRegsBaseAddr;
		emacBaseAddr->prussPru0CtrlRegs = 
			icss_EmacBaseAddrCfgParams[instance-1].prussPru0CtrlRegs;
		emacBaseAddr->prussPru1CtrlRegs = 
			icss_EmacBaseAddrCfgParams[instance-1].prussPru1CtrlRegs;
		emacBaseAddr->sharedDataRamBaseAddr = 
			icss_EmacBaseAddrCfgParams[instance-1].sharedDataRamBaseAddr;
	}
}

/**
 * @internal
 * @brief Registering Interrupts and Enabling global interrupts
 *
 * @param icssEmacHandle: ICSS_EMAC handler
 *
 * @retval none
 */
void InterruptInit(ICSS_EmacHandle icssEmacHandle)
{
	HwiP_Handle rxHwiHandle;
	HwiP_Handle linkHwiHandle;
	static uint32_t cookie = 0;
	uint8_t linkIntrN = (((ICSS_EmacObject*)
			icssEmacHandle->object)->emacInitcfg)->linkIntNum;
	uint8_t rxIntrN = (((ICSS_EmacObject*)
			icssEmacHandle->object)->emacInitcfg)->rxIntNum;

	cookie = ICSS_EMAC_osalHardwareIntDisable();

	HwiP_Params hwiParams;

	ICSS_EMAC_osalHwiParamsInit(&hwiParams);

	hwiParams.arg = (uintptr_t)icssEmacHandle;
	hwiParams.evtId = rxIntrN;
	hwiParams.priority = 0x20; 
	rxHwiHandle = ICSS_EMAC_osalRegisterInterrupt(rxIntrN, 
			(HwiP_Fxn)ICSS_EmacRxInterruptHandler, &hwiParams);
	if (rxHwiHandle == NULL )
		return;

	hwiParams.arg = (uintptr_t)icssEmacHandle;
	hwiParams.evtId = linkIntrN;
	hwiParams.priority = 0x20;
	linkHwiHandle = ICSS_EMAC_osalRegisterInterrupt(linkIntrN,
			(HwiP_Fxn)ICSS_EmacLinkISR, &hwiParams);
	if (linkHwiHandle == NULL)
		return;

	((ICSS_EmacObject*)icssEmacHandle->object)->rxintHandle = rxHwiHandle;
	((ICSS_EmacObject*)icssEmacHandle->object)->linkintHandle = 
		linkHwiHandle;

	ICSS_EMAC_osalHardwareIntRestore(cookie);
}
/**
 * @internal
 * @brief De-registering the interrupts and disabling global interrupts
 *
 * @param none
 *
 * @retval none
 */
void InterruptEnd(ICSS_EmacHandle icssEmacHandle)
{
	ICSS_EMAC_osalHardwareIntDestruct((HwiP_Handle)(((ICSS_EmacObject*)
				icssEmacHandle->object)->rxintHandle));

	ICSS_EMAC_osalHardwareIntDestruct((HwiP_Handle)(((ICSS_EmacObject*)
				icssEmacHandle->object)->linkintHandle));
}
/**
 * @internal
 * @brief This function enables the EMAC interrupts
 *
 * @param none
 *
 * @retval none
 */
static void EnableEMACInterrupts(ICSS_EmacHandle icssemacHandle)
{
	uint32_t key = 0;

	ICSS_EmacHandle handle = icssemacHandle;
	key = ICSS_EMAC_osalHardwareIntDisable();

	ICSS_EMAC_osalHardwareInterruptEnable((((ICSS_EmacObject*)
				handle->object)->emacInitcfg)->linkIntNum);
	ICSS_EMAC_osalHardwareInterruptEnable((((ICSS_EmacObject*)
				handle->object)->emacInitcfg)->rxIntNum);

	ICSS_EMAC_osalHardwareIntRestore(key);
}
/**
 * @brief This function disables the EMAC interrupts
 * @internal
 * @param none
 *
 * @retval none
 */
void DisableEMACInterrupts(ICSS_EmacHandle icssemacHandle)
{
	uint32_t key;
	ICSS_EmacHandle handle = icssemacHandle;
	key = ICSS_EMAC_osalHardwareIntDisable();

	ICSS_EMAC_osalHardwareInterruptDisable((((ICSS_EmacObject*)
				handle->object)->emacInitcfg)->linkIntNum);
	ICSS_EMAC_osalHardwareInterruptDisable((((ICSS_EmacObject*)
				handle->object)->emacInitcfg)->rxIntNum);

	ICSS_EMAC_osalHardwareIntRestore(key);
}

/**
 * @brief Clears Link interrupt for Port0
 * @internal
 * @param pruIcssHandle Provides PRUSS memory map
 *
 * @retval none
 */
inline void clearLink0ISR(ICSS_EmacHandle icssemacHandle)
{
	HW_WR_FIELD32(((((ICSS_EmacHwAttrs *)icssemacHandle->hwAttrs)
			->emacBaseAddrCfg)->prussIntcRegs + CSL_ICSSINTC_SECR1),
			CSL_ICSSINTC_SECR1_ENA_STATUS_63_32, 0x200);
}

/**
 * @brief Clears Link interrupt for ICSS_EMAC_PORT_1
 * @internal
 * @param pruIcssHandle Provides PRUSS memory map
 *
 * @retval none
 */
inline void clearLink1ISR(ICSS_EmacHandle icssemacHandle)
{
	HW_WR_FIELD32(((((ICSS_EmacHwAttrs *)icssemacHandle->hwAttrs)
			->emacBaseAddrCfg)->prussIntcRegs + CSL_ICSSINTC_SECR1),
			CSL_ICSSINTC_SECR1_ENA_STATUS_63_32, 0x200000);
}
